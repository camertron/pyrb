package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public class StaticmethodDecorator implements IDecorator {
    private Python3Parser.FuncdefContext funcDef;
    private String contextRef;

    public StaticmethodDecorator(Python3Parser.FuncdefContext funcDef, String contextRef) {
        this.funcDef = funcDef;
        this.contextRef = contextRef;
    }

    public void applyTo(BaseContext context) {
        context.writeRuby(contextRef);
        context.writeRuby(".attrs['");
        context.writeRuby(funcDef.NAME().getText());
        context.writeRubyLine("'] = Pyrb.staticmethod.call(");
        context.enterBlock();
        context.writeRuby(contextRef);
        context.writeRuby(", ");
        context.enterScope(ScopeType.INVOCATION, false);
        new FuncDefVisitor(context).visit(funcDef);
        context.exitScope(false);
        context.exitBlock();
        context.writeRubyLine(")\n");
    }

    public IDecorator getSubDecorator(ArrayList<String> subNames, ParseTree body) {
        throw new RuntimeException("The staticmethod decorator doesn't have any sub-decorators");
    }
}
