package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class TestListCompVisitor extends StatementVisitor {
    public TestListCompVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitTestlist_comp(Python3Parser.Testlist_compContext ctx) {
        for(ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                context.writeRuby(Ruby.pad(child.getText()));
            } else {
                visit(child);
            }
        }

        return null;
    }
}
