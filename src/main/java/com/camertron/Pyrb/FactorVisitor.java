package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class FactorVisitor extends StatementVisitor {
    public FactorVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitFactor(Python3Parser.FactorContext ctx) {
        for (ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                // this handles negative signs on numbers, eg. -1
                context.writeRuby(child.getText());
            } else {
                visit(child);
            }
        }

        return null;
    }
}
