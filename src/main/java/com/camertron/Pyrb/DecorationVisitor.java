package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

import java.util.ArrayList;

public class DecorationVisitor extends StatementVisitor {
    public static String[] SUPPORTED_ANNOTATIONS = { "classmethod", "staticmethod", "property"};
    public DecorationVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitDecorated(Python3Parser.DecoratedContext ctx) {
        for (Python3Parser.DecoratorContext decorator : ctx.decorators().decorator()) {
            String name = decorator.dotted_name().NAME(0).getText();

            // built-in decorators, i.e. ones that don't live in some imported module
            // and therefore are only one name long
            if (decorator.dotted_name().NAME().size() == 1) {
                if (name.equals("property")) {
                    PropertyDecorator prop = new PropertyDecorator(ctx.funcdef(), "klass");
                    prop.applyTo(context);
                    context.getCurrentScope().addDecorator(ctx.funcdef().NAME().getText(), prop);
                } else if (name.equals("classmethod")) {
                    new ClassmethodDecorator(ctx.funcdef(), "klass").applyTo(context);
                } else if (name.equals("staticmethod")) {
                    new StaticmethodDecorator(ctx.funcdef(), "klass").applyTo(context);
                }
            } else if (context.getCurrentScope().hasDecorator(name)) {
                IDecorator dec = context.getCurrentScope().getDecorator(name);
                ArrayList<String> subNames = new ArrayList<String>();

                for (int i = 1; i < decorator.dotted_name().NAME().size(); i ++) {
                    subNames.add(decorator.dotted_name().NAME(i).getText());
                }

                dec.getSubDecorator(subNames, ctx.funcdef()).applyTo(context);
            }
        }

        return null;
    }

//    @Override
//    public Void visitDecorated(Python3Parser.DecoratedContext ctx) {
//        if (ctx.funcdef() != null) {
//            annotateFunctionDef(ctx);
//        } else {
//            throw new RuntimeException("Don't know how to handle this annotation");
//        }
//
//        return null;
//    }
//
//    private void annotateFunctionDef(Python3Parser.DecoratedContext ctx) {
//        if (context.getCurrentScope().getType() == ScopeType.CLASS) {
//            String funcName = ctx.funcdef().NAME().getText();
//            context.writeRuby("klass.attrs['");
//            context.writeRuby(funcName);
//            context.writeRuby("'] = ");
//            applyDecorators(ctx, "klass", funcName);
//        } else {
//            throw new RuntimeException("Encountered a function annotation in an unexpected place");
//        }
//    }
//
//    private void applyDecorators(Python3Parser.DecoratedContext ctx, String contextVar, String callableName) {
//        for (Python3Parser.DecoratorContext decorator : ctx.decorators().decorator()) {
//            for (int i = 0; i < decorator.dotted_name().NAME().size(); i ++) {
//                if (i == 0) {
//                    String annotationName = decorator.dotted_name().NAME().get(i).getText();
//
//                    if (isSupported(annotationName)) {
//                        context.writeRuby(context.getSymbol(annotationName).getRubyExpression());
//                    } else {
//                        context.writeRuby(contextVar);
//                        context.writeRuby(".annotations['");
//                        context.writeRuby(callableName);
//                        context.writeRuby("']");
//                    }
//                } else {
//                    context.writeRuby(".attrs['");
//                    context.writeRuby(decorator.dotted_name().NAME().get(i).getText());
//                    context.writeRuby("']");
//                }
//            }
//
//            context.writeRubyLine(".call(");
//            context.enterScope(ScopeType.INVOCATION);
//            context.writeRuby(contextVar);
//            context.writeRuby(", \"");
//            context.writeRuby(callableName);
//            context.writeRuby("\", ");
//        }
//
//        visitChildren(ctx);
//
//        for (int i = 0; i < ctx.decorators().decorator().size(); i ++) {
//            context.exitScope();
//            context.writeRubyLine(")\n");
//        }
//    }
//
//    private boolean isSupported(String annotationName) {
//        for (String ann : SUPPORTED_ANNOTATIONS) {
//            if (ann.equals(annotationName)) {
//                return true;
//            }
//        }
//
//        return false;
//    }
}
