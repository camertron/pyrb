package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.List;

public class ArgListVisitor extends StatementVisitor {
    private ArgumentList args;

    public ArgListVisitor(BaseContext context) {
        super(context);
        args = new ArgumentList();
    }

    @Override
    public Void visitArglist(Python3Parser.ArglistContext ctx) {
        for (int i = 0; i < ctx.argument().size(); i ++) {
            Python3Parser.ArgumentContext argCtx = ctx.argument(i);
            args.add(new Symbol(argCtx.getText(), Ruby.toIdentifier(argCtx.getText()), SymbolType.ARG), Argument.ArgType.ARG, false, false);
        }

        return null;
    }

    @Override
    public Void visitTypedargslist(Python3Parser.TypedargslistContext ctx) {
        visitGeneric(ctx.children);
        return null;
    }

    @Override
    public Void visitVarargslist(Python3Parser.VarargslistContext ctx) {
        visitGeneric(ctx.children);
        return null;
    }

    @Override
    public Void visitExprlist(Python3Parser.ExprlistContext ctx) {
        visitGeneric(ctx.children);
        return null;
    }

    private Void visitGeneric(List<ParseTree> nodes) {
        String curName = null;
        boolean splat = false;
        boolean kwsplat = false;
        String defaultValue = null;
        Argument.ArgType argType = Argument.ArgType.ARG;

        for (ParseTree node : nodes) {
            if (node.getClass() == Python3Parser.TfpdefContext.class || node.getClass() == Python3Parser.VfpdefContext.class || node.getClass() == Python3Parser.ExprContext.class) {
                // use the first child to skip over potential type annotations, which we don't support
                curName = node.getChild(0).getText();
            } else if (node.getClass() == Python3Parser.TestContext.class) {
                BaseContext defaultVal = new BaseContext(context.scopeStack);
                new StatementVisitor(defaultVal).visit(node);
                defaultValue = defaultVal.getRubyOutput();
                argType = Argument.ArgType.KW_ARG;
            } else if (node.getClass() == TerminalNodeImpl.class) {
                if (node.getText().equals("*")) {
                    splat = true;
                } else if (node.getText().equals("**")) {
                    kwsplat = true;
                } else if (node.getText().equals(",")) {
                    String varName;

                    if (curName == null) {
                        curName = "_";
                        varName = "_";
                    } else {
                        varName = Ruby.getUniqueVariableName(curName, context.getCurrentScope().getSymbolTable());
                    }

                    args.add(new Symbol(curName, varName, SymbolType.ARG), argType, splat, kwsplat, defaultValue);
                    curName = null;
                    splat = false;
                    kwsplat = false;
                    defaultValue = null;
                    argType = Argument.ArgType.ARG;
                }
            }
        }

        if (curName != null) {
            String varName = Ruby.getUniqueVariableName(curName, context.getCurrentScope().getSymbolTable());
            args.add(new Symbol(curName, varName, SymbolType.ARG), argType, splat, kwsplat, defaultValue);
        }

        return null;
    }

    public ArgumentList getArgs() {
        return args;
    }
}
