package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ExponentVisitor extends StatementVisitor {
    public ExponentVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitPower(Python3Parser.PowerContext ctx) {
        if (ctx.getChildCount() == 1) {
            visitChildren(ctx);
        } else {
            visit(ctx.children.get(0));
            context.writeRuby(" ** ");
            visit(ctx.children.get(2));
        }

        return null;
    }
}
