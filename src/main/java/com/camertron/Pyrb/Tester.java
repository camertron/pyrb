package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Lexer;
import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.*;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Paths;

public class Tester {
//    private static String indentNewlines(String str) {
//        return str
//                .replaceAll("\\r?\\n", "\n")
//                .replaceAll("\n[\\s]*\n", "\n\n")
//                .replaceAll("\n +\\z", "\n");
//    }

    public static void main(String[] args) throws IOException, InterruptedException {
//        String str = new String(Files.readAllBytes(Paths.get("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/heapq.rb")), StandardCharsets.UTF_8);
//        System.out.println(indentNewlines(str).replaceAll(" ", "0"));

//        System.out.println("abc\n   \ndef".replaceAll("\n[\\s]*\n", "\n\n").replaceAll(" ", "0"));

//        CharStream in = CharStreams.fromFileName("/Users/cameron/Exercism/python/hamming/hamming.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/Exercism/python/hamming/hamming_test.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/Exercism/python/hamming/unittest/test_case.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/Exercism/python/hamming/unittest/__init__.py");
        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/tester2.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/prop_tester.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/binary-search/binary_search.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/allergies/allergies.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/armstrong-numbers/armstrong_numbers.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/atbash-cipher/atbash_cipher.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/bob/bob.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/beer-song/beer.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/bracket-push/bracket_push.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/clock/clock.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/grade-school/school.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/beer-song/beer.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/matrix/matrix.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/minesweeper/minesweeper_test.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/nth-prime/prime.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/circular-buffer/circular_buffer_test.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/rail-fence-cipher/rail_fence_cipher.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/hamming/hamming.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/say/say.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/ocr-numbers/ocr.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/exercism/python/space-age/space_age.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/heapq.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/test_bisect.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/bisect.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/integration/source/wordy.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/integration/source/atbash_cipher.py");

//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/test_struct.py");

//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/urllib/parse.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/heapq.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/test_base64.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/test_struct.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/spec/integration/source/queen_attack.py");
//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/antlr4/runtime/Ruby/src/antlr4/atn/ATNDeserializer.py");

//        CharStream in = CharStreams.fromFileName("/Users/cameron/workspace/pyrb-runtime/lib/pyrb/getopt.py");

        Python3Lexer lexer = new Python3Lexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Python3Parser parser = new Python3Parser(tokens);
        Python3Parser.File_inputContext file = parser.file_input();
        FileContext context = new FileContext("tester.py");
        ImportExportScanner importExportScanner = new ImportExportScanner(context);
        importExportScanner.visit(file);
        importExportScanner.commit();
        StatementVisitor visitor = new StatementVisitor(context);
        visitor.visit(file);
        context.close();
        System.out.println(context.getRubyOutput());
        Thread.sleep(500);
    }
}
