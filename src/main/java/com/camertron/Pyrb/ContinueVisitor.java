package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ContinueVisitor extends StatementVisitor {
    public ContinueVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitContinue_stmt(Python3Parser.Continue_stmtContext ctx) {
        context.writeRubyLine("next");
        return null;
    }
}
