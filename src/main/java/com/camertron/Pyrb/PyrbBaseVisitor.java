package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3BaseVisitor;
import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

public class PyrbBaseVisitor extends Python3BaseVisitor<Void> {
    protected BaseContext context;

    public PyrbBaseVisitor(BaseContext context) {
        this.context = context;
    }

    protected Python3Parser.Atom_exprContext findNextAtomExpr(ParseTree ctx) {
        if (ctx.getClass() == Python3Parser.Atom_exprContext.class) {
            return (Python3Parser.Atom_exprContext)ctx;
        }

        for (int i = 0; i < ctx.getChildCount(); i ++) {
            Python3Parser.Atom_exprContext atom = findNextAtomExpr(ctx.getChild(i));

            if (atom != null) {
                return atom;
            }
        }

        return null;
    }

    protected ExpressionChild findChildInParentExpr(ParseTree ctx) {
        ParseTree current = ctx;
        ParseTree last = ctx;

        while (current != null) {
            if (current.getClass() == Python3Parser.Expr_stmtContext.class) {
                Python3Parser.Expr_stmtContext expr = (Python3Parser.Expr_stmtContext)current;
                return new ExpressionChild(expr, expr.children.indexOf(last));
            }

            last = current;
            current = current.getParent();
        }

        return null;
    }
}
