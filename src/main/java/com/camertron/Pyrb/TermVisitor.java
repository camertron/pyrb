package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class TermVisitor extends StatementVisitor {
    public TermVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitTerm(Python3Parser.TermContext ctx) {
        if (ctx.children.size() <= 1) {
            return visitChildren(ctx);
        }

        int totalIntDivs = countIntDivisions(ctx);

        if (totalIntDivs == 0) {
            context.writeRuby("(");
        } else {
            for (int c = 0; c < totalIntDivs; c++) {
                context.writeRuby("Pyrb.int_divide(");
            }
        }

        int openDivs = 0;

        for (int i = 0; i < ctx.children.size(); i += 2) {
            visit(ctx.children.get(i));
            String operator;

            if (i < ctx.children.size() - 1) {
                operator = ctx.children.get(i + 1).getText();
            } else {
                operator = "";
            }

            if (openDivs > 0) {
                context.writeRuby(")");
                openDivs --;
            }

            if (operator.equals("//")) {
                openDivs ++;
                context.writeRuby(", ");
            } else {
                context.writeRuby(Ruby.pad(operator));
            }
        }

        if (totalIntDivs == 0) {
            context.writeRuby(")");
        }

        return null;
    }

    private int countIntDivisions(Python3Parser.TermContext ctx) {
        int count = 0;

        for (ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class && child.getText().equals("//")) {
                count ++;
            }
        }

        return count;
    }
}
