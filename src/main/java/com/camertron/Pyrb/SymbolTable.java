package com.camertron.Pyrb;

import java.util.ArrayList;
import java.util.HashMap;

public class SymbolTable {
    private Scope scope;
    private HashMap<String, Symbol> symbols;
    private SymbolTable parent;

    private static SymbolTable baseTable;

    static {
        baseTable = new SymbolTable();
        baseTable.addSymbol(Symbol.builtIn("object", "Pyrb::Object"));
        baseTable.addSymbol(Symbol.builtIn("len", "Pyrb.len"));
        baseTable.addSymbol(Symbol.builtIn("max", "Pyrb.max"));
        baseTable.addSymbol(Symbol.builtIn("min", "Pyrb.min"));
        baseTable.addSymbol(Symbol.builtIn("range", "Pyrb.range"));
        baseTable.addSymbol(Symbol.builtIn("print", "Pyrb.print"));
        baseTable.addSymbol(Symbol.builtIn("hasattr", "Pyrb.hasattr"));
        baseTable.addSymbol(Symbol.builtIn("str", "Pyrb::Str"));
        baseTable.addSymbol(Symbol.builtIn("dict", "Pyrb::Dict"));
        baseTable.addSymbol(Symbol.builtIn("dir", "Pyrb.dir"));
        baseTable.addSymbol(Symbol.builtIn("bool", "Pyrb::Bool"));
        baseTable.addSymbol(Symbol.builtIn("ValueError", "Pyrb::ValueError"));
        baseTable.addSymbol(Symbol.builtIn("AttributeError", "Pyrb::AttributeError"));
        baseTable.addSymbol(Symbol.builtIn("KeyError", "Pyrb::KeyError"));
        baseTable.addSymbol(Symbol.builtIn("IndexError", "Pyrb::IndexError"));
        baseTable.addSymbol(Symbol.builtIn("TypeError", "Pyrb::TypeError"));
        baseTable.addSymbol(Symbol.builtIn("UnicodeError", "Pyrb::UnicodeError"));
        baseTable.addSymbol(Symbol.builtIn("OverflowError", "Pyrb::OverflowError"));
        baseTable.addSymbol(Symbol.builtIn("ImportError", "Pyrb::ImportError"));
        baseTable.addSymbol(Symbol.builtIn("ZeroDivisionError", "Pyrb::ZeroDivisionError"));
        baseTable.addSymbol(Symbol.builtIn("StopIteration", "::StopIteration"));
        baseTable.addSymbol(Symbol.builtIn("UnicodeEncodeError", "Pyrb::UnicodeEncodeError"));
        baseTable.addSymbol(Symbol.builtIn("DeprecationWarning", "Pyrb::DeprecationWarning"));
        baseTable.addSymbol(Symbol.builtIn("OSError", "Pyrb::OSError"));
        baseTable.addSymbol(Symbol.builtIn("True", "true"));
        baseTable.addSymbol(Symbol.builtIn("False", "false"));
        baseTable.addSymbol(Symbol.builtIn("isinstance", "Pyrb.isinstance"));
        baseTable.addSymbol(Symbol.builtIn("type", "Pyrb.type"));
        baseTable.addSymbol(Symbol.builtIn("None", "nil"));
        baseTable.addSymbol(Symbol.builtIn("__name__", "__name__"));
        baseTable.addSymbol(Symbol.builtIn("map", "Pyrb::Map"));
        baseTable.addSymbol(Symbol.builtIn("filter", "Pyrb::Filter"));
        baseTable.addSymbol(Symbol.builtIn("int", "Pyrb::Int"));
        baseTable.addSymbol(Symbol.builtIn("float", "Pyrb::Float"));
        baseTable.addSymbol(Symbol.builtIn("sum", "Pyrb.sum"));
        baseTable.addSymbol(Symbol.builtIn("list", "Pyrb::List"));
        baseTable.addSymbol(Symbol.builtIn("tuple", "Pyrb::List"));
        baseTable.addSymbol(Symbol.builtIn("set", "Pyrb::Set"));
        baseTable.addSymbol(Symbol.builtIn("frozenset", "Pyrb::Frozenset"));
        baseTable.addSymbol(Symbol.builtIn("bytes", "Pyrb::Bytes"));
        baseTable.addSymbol(Symbol.builtIn("bytearray", "Pyrb::Bytearray"));
        baseTable.addSymbol(Symbol.builtIn("memoryview", "Pyrb::Memoryview"));
        baseTable.addSymbol(Symbol.builtIn("sorted", "Pyrb.sorted"));
        baseTable.addSymbol(Symbol.builtIn("zip", "Pyrb::Zip"));
        baseTable.addSymbol(Symbol.builtIn("next", "Pyrb.next_"));
        baseTable.addSymbol(Symbol.builtIn("reversed", "Pyrb.reversed"));
        baseTable.addSymbol(Symbol.builtIn("enumerate", "Pyrb.enumerate"));
        baseTable.addSymbol(Symbol.builtIn("classmethod", "Pyrb.classmethod"));
        baseTable.addSymbol(Symbol.builtIn("staticmethod", "Pyrb.staticmethod"));
        baseTable.addSymbol(Symbol.builtIn("property", "Pyrb.property"));
        baseTable.addSymbol(Symbol.builtIn("Exception", "Pyrb::Exception"));
        baseTable.addSymbol(Symbol.builtIn("getattr", "Pyrb.getattr"));
        baseTable.addSymbol(Symbol.builtIn("callable", "Pyrb.callable"));
        baseTable.addSymbol(Symbol.builtIn("any", "Pyrb.any"));
        baseTable.addSymbol(Symbol.builtIn("all", "Pyrb.all"));
        baseTable.addSymbol(Symbol.builtIn("abs", "Pyrb.abs"));
        baseTable.addSymbol(Symbol.builtIn("divmod", "Pyrb.divmod"));
        baseTable.addSymbol(Symbol.builtIn("chr", "Pyrb.chr"));
        baseTable.addSymbol(Symbol.builtIn("ord", "Pyrb.ord"));
        baseTable.addSymbol(Symbol.builtIn("getattr", "Pyrb.getattr"));
        baseTable.addSymbol(Symbol.builtIn("setattr", "Pyrb.setattr"));
        baseTable.addSymbol(Symbol.builtIn("round", "Pyrb.round"));
        baseTable.addSymbol(Symbol.builtIn("eval", "Pyrb.eeval"));  // lord jesus forgive me
        baseTable.addSymbol(Symbol.builtIn("iter", "Pyrb.iter"));
        baseTable.addSymbol(Symbol.builtIn("repr", "Pyrb.repr"));
        baseTable.addSymbol(Symbol.builtIn("super", "Pyrb.zuper"));
        baseTable.addSymbol(Symbol.builtIn("issubclass", "Pyrb.issubclass"));
        baseTable.addSymbol(Symbol.builtIn("exit", "Pyrb.exit"));
        baseTable.addSymbol(Symbol.builtIn("open", "Pyrb.open"));
    }

    public static SymbolTable getBaseTable() {
        return baseTable;
    }

    public SymbolTable() {
        this.symbols = new HashMap<String, Symbol>();
    }

    public SymbolTable(SymbolTable parent) {
        this();
        this.parent = parent;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void overrideSymbol(Symbol symbol) {
        if (symbol.getType() == SymbolType.LOCAL && scope.isInScope(ScopeType.BLOCK)) {
            symbol.setType(SymbolType.BLOCK_LOCAL);
        }

        symbols.put(symbol.getPythonName(), symbol);
    }

    public void overrideSymbol(String pythonName, String rubyExpression) {
        overrideSymbol(new Symbol(pythonName, Ruby.toIdentifier(rubyExpression)));
    }

    public void addSymbol(Symbol symbol) {
        if (!symbols.containsKey(symbol.getPythonName())) {
            overrideSymbol(symbol);
        }
    }

    public void addSymbol(String pythonName, String rubyExpression) {
        addSymbol(new Symbol(pythonName, Ruby.toIdentifier(rubyExpression)));
    }

    public void addSymbol(String pythonName, String rubyExpression, SymbolType type) {
        overrideSymbol(new Symbol(pythonName, Ruby.toIdentifier(rubyExpression), type));
    }

    public void removeSymbol(String pythonName) {
        symbols.remove(pythonName);
    }

    public Symbol getSymbol(String pythonName) {
        if (symbols.containsKey(pythonName)) {
            return symbols.get(pythonName);
        } else if (parent != null) {
            return parent.getSymbol(pythonName);
        }

        return null;
    }

    public Symbol getGlobalSymbol(String pythonName) {
        // why is this the same as getSymbol()?
        if (symbols.containsKey(pythonName)) {
            return symbols.get(pythonName);
        } else if (parent != null) {
            return parent.getSymbol(pythonName);
        }

        return null;
    }

    public boolean contains(String pythonName) {
        return getSymbol(pythonName) != null;
    }

    public boolean containsRuby(String rubyExp) {
        for (Symbol sym : symbols.values()) {
            if (sym.getRubyExpression().equals(rubyExp)) {
                return true;
            }
        }

        if (parent != null) {
            return parent.containsRuby(rubyExp);
        }

        return false;
    }

    public ArrayList<Symbol> getBlockLocals() {
        ArrayList<Symbol> locals = new ArrayList<Symbol>();

        for (Symbol sym : symbols.values()) {
            if (sym.getType() == SymbolType.BLOCK_LOCAL) {
                locals.add(sym);
            }
        }

        return locals;
    }

    public SymbolTable getParent() {
        return parent;
    }
}
