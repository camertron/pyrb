package com.camertron.Pyrb;

public enum ScopeType {
    FILE, CLASS, METHOD, BLOCK, GENERATOR, INVOCATION, FOR_LOOP_WITH_ELSE
}
