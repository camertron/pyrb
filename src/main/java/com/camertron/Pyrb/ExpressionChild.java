package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ExpressionChild {
    public enum Side { LEFT, RIGHT }

    private Python3Parser.Expr_stmtContext parent;

    private int childIndex;

    public ExpressionChild(Python3Parser.Expr_stmtContext parent, int childIndex) {
        this.parent = parent;
        this.childIndex = childIndex;
    }

    public Python3Parser.Expr_stmtContext getParent() {
        return parent;
    }

    public int getChildIndex() {
        return childIndex;
    }

    public String getOperator() {
        int operatorIndex = 0;

        if (childIndex == 0) {
            operatorIndex = 1;
        } else if (childIndex == parent.children.size() - 1) {
            operatorIndex = childIndex - 1;
        } else {
            operatorIndex = childIndex + 1;
        }

        if (operatorIndex <= parent.children.size() - 1) {
            return parent.children.get(operatorIndex).getText();
        } else {
            return "";
        }
    }

    public Side getSide() {
        if (childIndex == 0) {
            return Side.LEFT;
        } else if (childIndex == parent.children.size() - 1) {
            return Side.RIGHT;
        } else {
            return Side.LEFT;
        }
    }

    public boolean isAssignment() {
        return getOperator().endsWith("=");
    }
}
