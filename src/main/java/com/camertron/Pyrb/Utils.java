package com.camertron.Pyrb;

import java.util.Collection;

public class Utils {
    public static String join(Collection<String> items, String delimiter) {
        StringBuilder result = new StringBuilder();
        int counter = 0;

        for (String item : items) {
            if (counter > 0) {
                result.append(delimiter);
            }

            result.append(item);
            counter ++;
        }

        return result.toString();
    }

    public static String join(String[] items, String delimiter) {
        StringBuilder result = new StringBuilder();
        int counter = 0;

        for (String item : items) {
            if (counter > 0) {
                result.append(delimiter);
            }

            result.append(item);
            counter ++;
        }

        return result.toString();
    }
}
