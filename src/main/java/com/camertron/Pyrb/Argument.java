package com.camertron.Pyrb;

import java.util.HashMap;

public class Argument {
    enum ArgType { ARG, KW_ARG }

    private Symbol symbol;
    private ArgType type;
    private int position;
    private String defaultExp;
    private boolean splat;
    private boolean kwsplat;

    public Argument(Symbol symbol, ArgType type, int position, boolean splat, boolean kwsplat) {
        this.symbol = symbol;
        this.type = type;
        this.position = position;
        this.splat = splat;
        this.kwsplat = kwsplat;
        this.defaultExp = null;
    }

    public Argument(Symbol symbol, ArgType type, int position, boolean splat, boolean kwsplat, String defaultExp) {
        this(symbol, type, position, splat, kwsplat);
        this.defaultExp = defaultExp;
    }

    public String toParam() {
        StringBuilder result = new StringBuilder();

        if (splat) {
            result.append("*");
        } else if (kwsplat) {
            result.append("**");
        }

        result.append(symbol.getRubyExpression());

        if (defaultExp != null) {
            result.append(" = ");
            result.append(defaultExp);
        }

        return result.toString();
    }

    public String toMetadata() {
        HashMap<String, String> options = new HashMap<String, String>();
        StringBuilder metadata = new StringBuilder();

        metadata.append(symbol.getPythonName());
        metadata.append(": ");

        if (splat) {
            options.put("splat", "true");
        }

        if (kwsplat) {
            options.put("kwsplat", "true");
        }

        if (defaultExp != null) {
            options.put("default", defaultExp);
        }

        if (options.size() == 0) {
            metadata.append("nil");
        } else {
            int counter = 0;

            metadata.append("{ ");

            for (HashMap.Entry<String, String> entry : options.entrySet()) {
                if (counter > 0) {
                    metadata.append(", ");
                }

                metadata.append(entry.getKey());
                metadata.append(": ");
                metadata.append(entry.getValue());
                counter ++;
            }

            metadata.append(" }");
        }

        return metadata.toString();
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public ArgType getType() {
        return type;
    }

    public boolean isSplat() {
        return splat;
    }

    public int getPosition() {
        return position;
    }
}
