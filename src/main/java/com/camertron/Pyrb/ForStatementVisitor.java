package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class ForStatementVisitor extends StatementVisitor {
    public ForStatementVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitFor_stmt(Python3Parser.For_stmtContext ctx) {
        // Find closest atom so we know how to iterate over it (i.e. each vs. each_char).
        Python3Parser.Atom_exprContext iterableAtom = findNextAtomExpr(ctx.testlist());

        possiblyStartElseClause(ctx);

        if (ctx.testlist().test().size() > 1) {
            context.writeRuby("[");
        } else {
            context.writeRuby("(");
        }

        // enter in parens in order to call .each on the result
        visit(ctx.testlist());

        if (ctx.testlist().test().size() > 1) {
            context.writeRuby("]");
        } else {
            context.writeRuby(")");
        }

        if (ctx.testlist().test().size() == 1 && iterableAtom.atom().STRING(0) != null) {
            context.writeRuby(".each_char do |");
        } else {
            context.writeRuby(".each do |");
        }

        // print out all the arguments, i.e. for arg in foo
        ArgListVisitor argVisitor = new ArgListVisitor(context);
        argVisitor.visitExprlist(ctx.exprlist());

        for (Argument arg : argVisitor.getArgs()) {
            if (arg.getPosition() > 0) {
                context.writeRuby(", ");
            }

            context.writeRuby(arg.toParam());
        }

        context.writeRubyLine("|");

        context.enterScope(ScopeType.BLOCK);
        context = BlockContext.enter(context);

        for (Argument arg : argVisitor.getArgs()) {
            context.getSymbolTable().overrideSymbol(arg.getSymbol());
        }

        // For loops can have more than one suite if they include an "else", which is executed iff
        // no "break" is encountered.
        visit(ctx.suite(0));
        context = ((BlockContext)context).exit();
        context.exitScope();
        context.ensureNewLine();
        context.writeRubyLine("end");

        possiblyEndElseClause(ctx);

        return null;
    }

    @Override
    public Void visitComp_for(Python3Parser.Comp_forContext ctx) {
        if (ctx.parent.parent.getChild(0).getText().equals("{")) {
            constructDictOrSet(ctx);
        } else {
            constructList(ctx);
        }

        return null;
    }

    private void constructDictOrSet(Python3Parser.Comp_forContext ctx) {
        int oldScopeCount = context.scopeStack.size();
        boolean outermost = ctx.parent.getClass() != Python3Parser.Comp_iterContext.class;

        // print out the thing we're looping over, i.e. for foo in thing
        visit(ctx.or_test());

        if (outermost) {
            String injectable;

            if (((Python3Parser.DictorsetmakerContext)ctx.parent).test().size() == 1) {
                injectable = "Pyrb::Set.new";
            } else {
                injectable = "Pyrb::Dict.new";
            }

            context.writeRuby(".fcall('__iter__').each_with_object(" + injectable + ") do |(");
        } else {
            context.writeRuby(".fcall('__iter__').each do |");
        }

        context.enterScope(ScopeType.BLOCK);

        // print out all the arguments, i.e. for arg in foo
        ArgListVisitor argVisitor = new ArgListVisitor(context);
        argVisitor.visitExprlist(ctx.exprlist());

        for (Argument arg : argVisitor.getArgs()) {
            if (arg.getPosition() > 0) {
                context.writeRuby(", ");
            }

            context.writeRuby(arg.toParam());
            context.getSymbolTable().overrideSymbol(arg.getSymbol());
        }

        // close block args and enter loop scope
        if (outermost) {
            context.writeRubyLine("), _ret_|");
        } else {
            context.writeRubyLine("|");
        }

        // visit any other for statements chained off this one
        if (ctx.comp_iter() != null && ctx.comp_iter().comp_for() != null) {
            visit(ctx.comp_iter().comp_for());
        }

        // if this is the start of the chain, print out the return value (i.e. return_value for foo in bar)
        // and close all the scopes we've opened handling all the potentially chained loops
        if (outermost) {
            // find the conditional
            Python3Parser.Comp_ifContext conditional = findConditional(ctx);
            boolean isConditional = conditional != null;

            if (isConditional) {
                // for an explanation as to why we double-negate the conditional, see the comment
                // in the IfStatementVisitor
                context.writeRuby("if !!(");
                visit(conditional);
                context.writeRubyLine(")");
                context.enterBlock();
            }

            Python3Parser.DictorsetmakerContext parent = (Python3Parser.DictorsetmakerContext)ctx.parent;

            if (parent.test().size() == 1) {
                context.writeRuby("_ret_ << ");
                visit(parent.test(0));
            } else {
                context.writeRuby("_ret_[");
                visit(parent.test(0));
                context.writeRuby("] = ");
                visit(parent.test(1));
            }

            context.ensureNewLine();

            // exit the conditional
            if (isConditional) {
                context.exitBlock();
                context.writeRubyLine("end");
            }

            int newScopeCount = context.scopeStack.size();

            // Close all the open blocks.
            // We have to do this at the end because the outermost loop is the one that contains the test, meaning
            // by the time we get here the inner loops will have already been closed. Instead we intentionally leave
            // the chained loops open and leave it to the outermost loop to close everything based on how many scopes
            // were entered.
            for (int i = 0; i < newScopeCount - oldScopeCount; i ++) {
                context.exitScope();
                context.writeRubyLine("end");
            }
        }
    }

    private void constructList(Python3Parser.Comp_forContext ctx) {
        int oldScopeCount = context.scopeStack.size();
        boolean isConditional = false;

        // print out the thing we're looping over, i.e. for foo in thing
        visit(ctx.or_test());

        context.writeRuby(".fcall('__iter__')");

        if (ctx.comp_iter() != null) {
            if (ctx.comp_iter().comp_if() != null) {
                // for loop with a conditional
                isConditional = true;
                context.writeRuby(".each_with_object([]) do |");
            } else if (ctx.comp_iter().comp_for() != null) {
                // for loop with a chained loop after
                context.writeRuby(".flat_map do |");
            } else {
                // for loop with no chained loop OR conditional
                context.writeRuby(".map do |");
            }
        } else {
            // for loop with no conditional or chained loop...
            context.writeRuby(".map do |");
        }

        context.enterScope(ScopeType.BLOCK);

        // enable arg deconstruction
        if (ctx.exprlist().expr().size() > 1) {
            context.writeRuby("(");
        }

        // print out all the arguments, i.e. for arg in foo
        for (ParseTree child : ctx.exprlist().children) {
            if (child.getClass() == Python3Parser.ExprContext.class) {
                // Find the atom and loop over its children, emitting exactly what we find. We do this in the interest
                // of argument deconstruction. If the exprlist's only element is a tuple, then the length of the list
                // itself is only 1 and will not cause the above code to emit enclosing parens. Instead we have to look
                // at the closest atom (i.e. the arguments passed to the loop's block). It is not sufficient to simply
                // call visit() on the child because the atom expression visitor is designed to turn what it thinks are
                // tuples into arrays, so `for (first, second) in foo` becomes `foo.each do |[first, second]|`. To
                // prevent this, we emit what Python considers tuple parens and what Ruby thinks are arg deconstruction
                // parens (fortunately they happen to be equivalent in this case). As a result, we end up emitting
                // `foo.each do |(first, second)|`.
                Python3Parser.Atom_exprContext atomExpr = findNextAtomExpr(child);

                for (ParseTree atomChild : atomExpr.atom().children) {
                    if (atomChild.getClass() == TerminalNodeImpl.class) {
                        context.writeRuby(atomChild.getText());
                    } else {
                        visit(atomChild);
                    }
                }
            } else if (child.getClass() == TerminalNodeImpl.class) {
                // handle things like commas, splats, etc
                context.writeRuby(Ruby.pad(child.getText()));
            }
        }

        if (ctx.exprlist().expr().size() > 1) {
            context.writeRuby(")");
        }

        // Conditional for loops validate before returning. To support this case, we use each_with_object([]) and
        // conditionally add items to the _ret_ array.
        if (isConditional) {
            context.writeRuby(", _ret_");
        }

        // close block args and enter loop scope
        context.writeRubyLine("|");

        // visit any other for statements chained off this one
        if (ctx.comp_iter() != null && ctx.comp_iter().comp_for() != null) {
            visit(ctx.comp_iter().comp_for());
        }

        // if this is the start of the chain, print out the return value (i.e. return_value for foo in bar)
        // and close all the scopes we've opened handling all the potentially chained loops
        if (ctx.parent.getClass() != Python3Parser.Comp_iterContext.class) {
            // find the conditional
            Python3Parser.Comp_ifContext conditional = findConditional(ctx);
            isConditional = conditional != null;

            if (isConditional) {
                // for an explanation as to why we double-negate the conditional, see the comment
                // in the IfStatementVisitor
                context.writeRuby("if !!(");
                visit(conditional);
                context.writeRubyLine(")");
                context.enterBlock();
                context.writeRuby("_ret_ << ");
            }

            if (ctx.parent.getChild(0).getClass() == Python3Parser.TestContext.class) {
                visit(ctx.parent.getChild(0));
            }

            context.ensureNewLine();

            // exit the conditional
            if (isConditional) {
                context.exitBlock();
                context.writeRubyLine("end");
            }

            int newScopeCount = context.scopeStack.size();

            // Close all the open blocks.
            // We have to do this at the end because the outermost loop is the one that contains the test, meaning
            // by the time we get here the inner loops will have already been closed. Instead we intentionally leave
            // the chained loops open and leave it to the outermost loop to close everything based on how many scopes
            // were entered.
            for (int i = 0; i < newScopeCount - oldScopeCount; i ++) {
                context.exitScope();
                context.writeRubyLine("end");
            }
        }
    }

    private Python3Parser.Comp_ifContext findConditional(Python3Parser.Comp_forContext ctx) {
        Python3Parser.Comp_forContext innermost = ctx;

        while (innermost.comp_iter() != null && innermost.comp_iter().comp_for() != null) {
            innermost = innermost.comp_iter().comp_for();
        }

        if (innermost.comp_iter() != null) {
            return innermost.comp_iter().comp_if();
        }

        return null;
    }

    private void possiblyStartElseClause(Python3Parser.For_stmtContext ctx) {
        // indicates a for statement with an else clause, enter with catch
        if (ctx.suite().size() > 1) {
            context.writeRubyLine("catch(:break) do");
            context.enterScope(ScopeType.FOR_LOOP_WITH_ELSE);
        }
    }

    private void possiblyEndElseClause(Python3Parser.For_stmtContext ctx) {
        if (ctx.suite().size() > 1) {
            // causes the catch() to return false if no break was encountered
            context.writeRubyLine("false");
            context.exitScope();
            context.writeRubyLine("end.tap do |_caught|");
            context.enterScope(ScopeType.BLOCK);
            context.writeRubyLine("next if _caught");
            visit(ctx.suite(1));
            context.exitScope();
            context.writeRubyLine("end\n");
        }
    }
}
