package com.camertron.Pyrb;

import java.util.ArrayList;
import java.util.Arrays;

public class Python {
    // must start with a letter or underscore
    private static final String IDENT_RE = "[a-zA-Z_][a-zA-Z0-9_]*";

    // tokens that look like identifiers but are actually special keywords
    private static final ArrayList<String> KEYWORDS;

    static {
        KEYWORDS = new ArrayList<String>();
        KEYWORDS.addAll(Arrays.asList("None", "True", "False"));
    }

    public static boolean isIdentifier(String str) {
        return str.matches(IDENT_RE) && !KEYWORDS.contains(str);
    }
}
