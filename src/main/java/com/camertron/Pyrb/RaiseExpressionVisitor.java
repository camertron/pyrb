package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class RaiseExpressionVisitor extends StatementVisitor {
    public RaiseExpressionVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitRaise_stmt(Python3Parser.Raise_stmtContext ctx) {
        context.writeRuby("Pyrb.raize");

        if (ctx.test().size() > 0) {
            context.writeRuby("(");

            // object to raise
            visit(ctx.test(0));

            // "from" part of the raise (sets the "cause" in rubyland)
            if (ctx.test().size() > 1) {
                context.writeRuby(", from: ");
                visit(ctx.test(1));
            }

            context.writeRubyLine(")");
        } else {
            context.writeRubyLine();
        }

        return null;
    }
}
