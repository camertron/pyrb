package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class TestVisitor extends StatementVisitor {
    public TestVisitor(BaseContext context) {
        super(context);
    }

    public Void visitTest(Python3Parser.TestContext ctx) {
        // inline if, i.e. "var if foo else baz"
        // why this does not come through with regular if statements is a mystery
        if (ctx.children.size() > 1 && ctx.children.get(1).getText().equals("if")) {
            context.writeRuby("!!(");
            visit(ctx.children.get(2));
            context.writeRuby(")");
            context.writeRuby(" ? ");
            visit(ctx.children.get(0));
            context.writeRuby(" : ");
            visit(ctx.children.get(4));
        } else {
            visitChildren(ctx);
        }

        return null;
    }

    @Override
    public Void visitOr_test(Python3Parser.Or_testContext ctx) {
        if (ctx.getChildCount() == 1) {
            this.visitChildren(ctx);
        } else {
            for (int i = 0; i < ctx.and_test().size(); i ++) {
                Python3Parser.ComparisonContext comp = ctx.and_test(i).not_test(0).comparison();

                if (i > 0) {
                    context.writeRuby(".or { ");
                } else {
                    if (comp != null && comp.children.size() > 1) {
                        context.writeRuby("(");
                    }
                }

                visit(ctx.and_test(i));

                if (i > 0) {
                    context.writeRuby(" }");
                } else {
                    if (comp != null && comp.children.size() > 1) {
                        context.writeRuby(")");
                    }
                }
            }
        }

        return null;
    }

    @Override
    public Void visitAnd_test(Python3Parser.And_testContext ctx) {
        if (ctx.getChildCount() == 1) {
            this.visitChildren(ctx);
        } else {
            for (int i = 0; i < ctx.not_test().size(); i ++) {
                Python3Parser.ComparisonContext comp = ctx.not_test(i).comparison();

                if (i > 0) {
                    context.writeRuby(".and { ");
                } else {
                    if (comp != null && comp.children.size() > 1) {
                        context.writeRuby("(");
                    }
                }

                visit(ctx.not_test(i));

                if (i > 0) {
                    context.writeRuby(" }");
                } else {
                    if (comp != null && comp.children.size() > 1) {
                        context.writeRuby(")");
                    }
                }
            }
        }

        return null;
    }

    @Override
    public Void visitNot_test(Python3Parser.Not_testContext ctx) {
        if (ctx.getChildCount() == 1) {
            return this.visitChildren(ctx);
        } else {
            context.writeRuby("(");
            visit(ctx.children.get(1));
            context.writeRuby(").not");
        }

        return null;
    }
}
