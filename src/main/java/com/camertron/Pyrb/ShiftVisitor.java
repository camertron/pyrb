package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class ShiftVisitor extends StatementVisitor {
    public ShiftVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitShift_expr(Python3Parser.Shift_exprContext ctx) {
        if (ctx.children.size() > 1) {
            for (ParseTree child : ctx.children) {
                if (child.getClass() == TerminalNodeImpl.class) {
                    context.writeRuby(Ruby.pad(child.getText()));
                } else {
                    visit(child);
                }
            }
        } else {
            visitChildren(ctx);
        }

        return null;
    }
}
