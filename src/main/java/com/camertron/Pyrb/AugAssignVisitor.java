package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class AugAssignVisitor extends StatementVisitor {
    public AugAssignVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitAugassign(Python3Parser.AugassignContext ctx) {
        context.writeRuby(Ruby.pad(ctx.children.get(0).getText()));
        return null;
    }
}
