package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Lexer;
import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length == 1 && args[0].equals("-")) {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            StringBuilder pythonCode = new StringBuilder();
            String line;

            while ((line = in.readLine()) != null) {
                pythonCode.append(line);
                pythonCode.append("\n");
            }

            String rubyCode = transpile("main.py", pythonCode.toString());
            System.out.println(rubyCode);
        } else {
            for (String pyFile : args) {
                System.out.print("Processing " + pyFile + "...");

                String pythonCode = new String(Files.readAllBytes(Paths.get(pyFile)), StandardCharsets.UTF_8);
                String rubyCode = transpile(pyFile, pythonCode);
                String outputFile = pyFile.substring(0, pyFile.length() - 3) + ".rb";
                FileOutputStream ostream = new FileOutputStream(outputFile);
                PrintWriter writer = new PrintWriter(ostream);
                writer.write(rubyCode);
                writer.close();
                ostream.close();

                System.out.println(" done");
            }
        }
    }

    private static String transpile(String pythonFilename, String pythonCode) {
        CharStream in = CharStreams.fromString(pythonCode);
        Python3Lexer lexer = new Python3Lexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Python3Parser parser = new Python3Parser(tokens);
        Python3Parser.File_inputContext file = parser.file_input();
        FileContext context = new FileContext(pythonFilename);
        ImportExportScanner importExportScanner = new ImportExportScanner(context);
        importExportScanner.visit(file);
        importExportScanner.commit();
        StatementVisitor visitor = new StatementVisitor(context);
        visitor.visit(file);
        context.close();

        return context.getRubyOutput();
    }
}
