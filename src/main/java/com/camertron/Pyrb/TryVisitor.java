package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class TryVisitor extends StatementVisitor {
    public TryVisitor(BaseContext context) {
        super(context);
    }

    public Void visitTry_stmt(Python3Parser.Try_stmtContext ctx) {
        for (ParseTree child : ctx.children) {
            if (child.getClass() == Python3Parser.SuiteContext.class) {
                context.enterBlock();
                context.setInsideTry(true);
                visit(child);
                context.setInsideTry(false);
                context.exitBlock();
            } else if (child.getClass() == Python3Parser.Except_clauseContext.class) {
                Python3Parser.Except_clauseContext except = (Python3Parser.Except_clauseContext)child;

                if (except.test() == null) {
                    context.writeRuby("rescue");
                } else {
                    context.writeRuby("rescue *Pyrb.rezcue(");
                    visit(except.test());
                    context.writeRuby(")");

                    if (except.NAME() != null) {
                        context.writeRuby(" => ");
                        context.writeRuby(except.NAME().toString());
                    }
                }

                context.writeRubyLine();
            } else if (child.getClass() == TerminalNodeImpl.class) {
                String keyword = child.getText();

                if (keyword.equals("try")) {
                    context.writeRubyLine("begin");
                } else if (keyword.equals("finally")) {
                    context.writeRubyLine("ensure");
                } else if (keyword.equals("else")) {
                    context.writeRubyLine("else");
                }
            }
        }

        context.writeRubyLine("end\n");

        return null;
    }
}
