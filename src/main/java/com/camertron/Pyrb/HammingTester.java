package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Lexer;
import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;
import java.io.PrintWriter;

public class HammingTester {
    public static void main(String[] args) throws IOException {
        transpile("/Users/cameron/Exercism/python/hamming/unittest/__init__.py", "/Users/cameron/Exercism/python/hamming/unittest/__init__.rb", "unittest/__init__.rb");
        transpile("/Users/cameron/Exercism/python/hamming/unittest/test_case.py", "/Users/cameron/Exercism/python/hamming/unittest/test_case.rb", "unittest/test_case.rb");
        transpile("/Users/cameron/Exercism/python/hamming/hamming.py", "/Users/cameron/Exercism/python/hamming/hamming.rb", "hamming.rb");
        transpile("/Users/cameron/Exercism/python/hamming/hamming_test.py", "/Users/cameron/Exercism/python/hamming/hamming_test.rb", "hamming_test.rb");
    }

    private static void transpile(String inputFile, String outputFile, String modulePath) throws IOException {
        CharStream in = CharStreams.fromFileName(inputFile);
        Python3Lexer lexer = new Python3Lexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Python3Parser parser = new Python3Parser(tokens);
        Python3Parser.File_inputContext file = parser.file_input();
        FileContext context = new FileContext(modulePath);
        StatementVisitor visitor = new StatementVisitor(context);
        visitor.visit(file);
        context.close();

        PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
        writer.println(context.getRubyOutput());
        writer.close();
    }
}
