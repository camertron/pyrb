package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.ArrayList;

public class ImportExportScanner extends PyrbBaseVisitor {
    private ImportVisitor importVisitor;

    public ImportExportScanner(FileContext context) {
        super(context);
        this.importVisitor = new ImportVisitor();
    }

    public void commit() {
        for (Import imp : importVisitor.getImports()) {
            Import importWithContext = imp.applyTo(context);
            context.writeRubyLine(importWithContext.toRuby());
        }

        if (importVisitor.getImports().size() > 0) {
            context.writeRubyLine();
        }
    }

    @Override
    public Void visitClassdef(Python3Parser.ClassdefContext ctx) {
        String className = ctx.NAME().getText();
        context.getSymbolTable().addSymbol(className, "exports['" + className + "']", SymbolType.CLASS);
        return null;
    }

    @Override
    public Void visitFuncdef(Python3Parser.FuncdefContext ctx) {
        String name = ctx.NAME().getText();
        context.getSymbolTable().addSymbol(name, "exports['" + name + "']", SymbolType.DEF);
        return null;
    }

    @Override
    public Void visitExpr_stmt(Python3Parser.Expr_stmtContext ctx) {
        if (isAssignment(ctx)) {
            String name = ctx.children.get(0).getText();
            context.getSymbolTable().addSymbol(name, "exports['" + name + "']");
        }

        return null;
    }

    @Override
    public Void visitTry_stmt(Python3Parser.Try_stmtContext ctx) {
        // don't let visitor descend into try/except block; leave those imports alone
        return null;
    }

    @Override
    public Void visitImport_stmt(Python3Parser.Import_stmtContext ctx) {
        importVisitor.visitImport_stmt(ctx);
        return null;
    }

    public ArrayList<Import> getImports() {
        return importVisitor.getImports();
    }

    private boolean isAssignment(Python3Parser.Expr_stmtContext ctx) {
        return ctx.children.size() > 1 &&
               ctx.children.get(1).getClass() == TerminalNodeImpl.class &&
               ctx.children.get(1).getText().equals("=") &&
               Python.isIdentifier(ctx.children.get(0).getText());
    }
}
