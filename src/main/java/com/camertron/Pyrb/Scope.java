package com.camertron.Pyrb;

import java.util.HashMap;

public class Scope {
    private ScopeType type;
    private SymbolTable symbolTable;
    private Scope parent;
    private HashMap<String, IDecorator> decorators;
    private Symbol selfRef;
    private Symbol classRef;

    public Scope(ScopeType type, SymbolTable symbolTable) {
        this.type = type;
        this.symbolTable = symbolTable;
        this.decorators = new HashMap<String, IDecorator>();
    }

    public Scope(ScopeType type, Scope parent) {
        this(type, new SymbolTable(parent.symbolTable));
        symbolTable.setScope(this);
        this.parent = parent;
        this.decorators = new HashMap<String, IDecorator>();
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public ScopeType getType() {
        return type;
    }

    public Scope getParent() {
        return parent;
    }

    public void addDecorator(String name, IDecorator decorator) {
        decorators.put(name, decorator);
    }

    public boolean hasDecorator(String name) {
        return decorators.containsKey(name);
    }

    public IDecorator getDecorator(String name) {
        return decorators.get(name);
    }

    public boolean isInScope(ScopeType type) {
        return type == this.type || (parent != null && parent.isInScope(type));
    }

    public void setSelfRef(Symbol selfRef) {
        this.selfRef = selfRef;
    }

    public void setClassRef(Symbol classRef) {
        this.classRef = classRef;
    }

    public Symbol getSelfRef() {
        if (selfRef == null) {
            if (parent == null) {
                return null;
            } else {
                return parent.getSelfRef();
            }
        } else {
            return selfRef;
        }
    }

    public Symbol getClassRef() {
        if (classRef == null) {
            if (parent == null) {
                return null;
            } else {
                return parent.getClassRef();
            }
        } else {
            return classRef;
        }
    }

    public Scope getInnermostLocalScope() {
        switch (getType()) {
            case FILE:
            case CLASS:
            case METHOD:
                return this;

            default:
                if (getParent() == null) {
                    return null;
                } else {
                    return getParent().getInnermostLocalScope();
                }
        }
    }
}
