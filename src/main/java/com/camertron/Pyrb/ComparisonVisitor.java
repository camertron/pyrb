package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ComparisonVisitor extends StatementVisitor {
    public ComparisonVisitor(BaseContext context) {
        super(context);
    }

    public Void visitComparison(Python3Parser.ComparisonContext ctx) {
        String operator = ctx.children.get(1).getText();

        if (operator.endsWith("in")) {
            if (operator.equals("notin")) {
                context.writeRuby("!");
            }

            context.writeRuby("(");
            visit(ctx.expr(0));
            context.writeRuby(").in?(");
            visit(ctx.expr(1));
            context.writeRuby(")");
        } else if (operator.startsWith("is")) {
            visit(ctx.expr(0));
            context.writeRuby(".object_id");

            if (operator.equals("is")) {
                context.writeRuby(" == ");
            } else {
                context.writeRuby(" != ");
            }

            visit(ctx.expr(1));
            context.writeRuby(".object_id");
        } else {
            for (int i = 1; i < ctx.expr().size(); i ++) {
                if (i > 1) {
                    context.writeRuby(" && ");  // @TODO: revisit this logic
                }

                visit(ctx.expr(i - 1));
                context.writeRuby(Ruby.pad(ctx.comp_op(i - 1).getText()));
                visit(ctx.expr(i));
            }
        }

        return null;
    }
}
