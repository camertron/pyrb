package com.camertron.Pyrb;

import java.util.ArrayList;
import java.util.Iterator;

public class ArgumentList implements Iterable<Argument> {
    private ArrayList<Argument> args;

    public ArgumentList() {
        this.args = new ArrayList<Argument>();
    }

    public void add(Symbol symbol, Argument.ArgType type, boolean splat, boolean kwsplat) {
        args.add(new Argument(symbol, type, args.size(), splat, kwsplat));
    }

    public void add(Symbol symbol, Argument.ArgType type, boolean splat, boolean kwsplat, String defaultExp) {
        args.add(new Argument(symbol, type, args.size(), splat, kwsplat, defaultExp));
    }

    public String toArgs() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < args.size(); i ++) {
            if (i > 0) {
                builder.append(", ");
            }

            builder.append(args.get(i).getSymbol().getRubyExpression());
        }

        return builder.toString();
    }

    public String toMetadata() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < args.size(); i ++) {
            if (i > 0) {
                builder.append(", ");
            }

            builder.append(args.get(i).toMetadata());
        }

        return builder.toString();
    }

    public Iterator<Argument> iterator() {
        return args.iterator();
    }

    public Argument get(int index) {
        return args.get(index);
    }

    public int size() {
        return args.size();
    }
}
