package com.camertron.Pyrb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Ruby {
    // these are array lists instead of arrays because I want to be able to call .contains() on them
    public static final ArrayList<String> LEFT_RIGHT_PAD;
    public static final ArrayList<String> RIGHT_PAD;
    public static final ArrayList<String> KEYWORDS;
    public static final ArrayList<String> KERNEL_FUNCTIONS;
    public static final String[][] RAW_STRING_WRAPPERS;

    static {
        LEFT_RIGHT_PAD = new ArrayList<String>();
        LEFT_RIGHT_PAD.addAll(Arrays.asList(
            "=", "==", "!=", ">", "<", ">=", "<=", "+", "-", "/", "*", "^", "%", "|", "&",
            "+=", "-=", "*=", "/=", "<<", ">>", "<<=", ">>="
        ));

        RIGHT_PAD = new ArrayList<String>();
        RIGHT_PAD.addAll(Arrays.asList(","));

        KEYWORDS = new ArrayList<String>();
        KEYWORDS.addAll(Arrays.asList(
            "__ENCODING__", "__LINE__", "__FILE__", "BEGIN", "END", "alias", "and",
            "begin", "break", "case", "class", "def", "defined?", "do", "else", "elsif",
            "end", "ensure", "false", "for", "if", "in", "module", "next", "nil", "not",
            "or", "redo", "rescue", "retry", "return", "self", "super", "then", "true",
            "undef", "unless", "until", "when", "while", "yield", "proc"
        ));

        KERNEL_FUNCTIONS = new ArrayList<String>();
        KERNEL_FUNCTIONS.addAll(Arrays.asList(
            "format"
        ));

        // This can technically be any pair of non-alphanumeric characters.
        // For example, %q(foo) is just as valid as %q+foo+
        // Let's use these for now.
        RAW_STRING_WRAPPERS = new String[][] { {"(", ")"}, {"[", "]"}, {"{", "}"}, {"|", "|"}, {"<", ">"} };
    }

    // regex to find unescaped # chars
    private static final Pattern INTERPOLATION_RE = Pattern.compile("(?<!\\\\)#");

    public static String constantize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static String modularize(String str) {
        return constantize(str.replaceAll("\\.py\\z", "").replaceAll("[-:\\\\/.]", "_"));
    }

    public static String toIdentifier(String str) {
        if (KEYWORDS.contains(str) || KERNEL_FUNCTIONS.contains(str)) {
            return str + "_";
        } else {
            return str;
        }
    }

    public static String pad(String str) {
        if (LEFT_RIGHT_PAD.contains(str)) {
            return " " + str + " ";
        } else if (RIGHT_PAD.contains(str)) {
            return str + " ";
        } else {
            return str;
        }
    }

    public static String wrapRawString(String raw) {
        for (String[] wrapper : RAW_STRING_WRAPPERS) {
            if (!raw.contains(wrapper[0]) && !raw.contains(wrapper[1])) {
                return "%q" + wrapper[0] + raw + wrapper[1];
            }
        }

        throw new RuntimeException("Could not find a way to wrap the raw string " + raw);
    }

    public static String escapeInterpolationChars(String str) {
        if (str.equals("#")) {
            return str;
        }

        String[] chunks = INTERPOLATION_RE.split(str);
        StringBuilder result = new StringBuilder(chunks[0]);

        for (int i = 1; i < chunks.length; i ++) {
            result.append("\\#");
            result.append(chunks[i]);
        }

        // String.split doesn't include the right-hand side of the split if it's empty
        if (str.endsWith("#")) {
            result.append("#");
        }

        return result.toString();
    }

    public static String escapeDoubleQuotedString(String str) {
        str = escapeInterpolationChars(str);
        StringBuilder result = new StringBuilder();
        boolean escaping = false;

        for (int i = 0; i < str.length(); i ++) {
            switch (str.charAt(i)) {
                case '\\':
                    if (escaping) {
                        // indicates escaped backslash
                        result.append("\\\\");
                        escaping = false;
                    } else {
                        escaping = true;
                    }

                    break;

                case '"':
                    result.append("\\\"");
                    escaping = false;
                    break;

                default:
                    if (escaping) {
                        // indicates escaped character of some kind (i.e. \n or \t)
                        result.append("\\");
                        escaping = false;
                    }

                    result.append(str.charAt(i));
                    break;
            }
        }

        return result.toString();
    }

    public static String getUniqueVariableName(String originalName, SymbolTable symbolTable) {
        String originalIdent = toIdentifier(originalName);

        if (!symbolTable.containsRuby(originalIdent)) {
            return originalIdent;
        }

        int counter = 2;
        String current;

        do {
            current = toIdentifier(originalName + counter);
            counter ++;
        } while (symbolTable.containsRuby(current));

        return current;
    }

    public static boolean isConstant(String ident) {
        // checking if first character is uppercase
        return ident.charAt(0) >= 65 && ident.charAt(0) <= 90;
    }
}