package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.*;

public class ImportVisitor extends StatementVisitor {
    private ArrayList<Import> imports;

    public ImportVisitor() {
        super(null);
        imports = new ArrayList<Import>();
    }

    public ArrayList<Import> getImports() {
        return imports;
    }

    @Override
    public Void visitImport_stmt(Python3Parser.Import_stmtContext ctx) {
        HashMap<String, String> importables = new LinkedHashMap<String, String>();

        // I have no idea what's going on here
        if (ctx.import_name() != null) {
            for (Python3Parser.Dotted_as_nameContext asName : ctx.import_name().dotted_as_names().dotted_as_name()) {
                String importName;
                String aliasName;

                if (asName.children.size() > 1 && asName.children.get(asName.children.size() - 2).getText().equals("as")) {
                    aliasName = asName.children.get(asName.children.size() - 1).getText();
                    importName = asName.dotted_name().getText();
                    importables.put(importName, aliasName);
                } else {
                    aliasName = asName.dotted_name().getText();
                    importName = aliasName;

                    int lastDot = importName.lastIndexOf(".");

                    if (lastDot == -1) {
                        importables.put(aliasName, importName);
                    } else {
                        importables.put(importName, importName.substring(lastDot + 1));
                    }
                }
            }
        } else if (ctx.import_from().import_as_names() != null) {
            for (Python3Parser.Import_as_nameContext imp : ctx.import_from().import_as_names().import_as_name()) {
                String name = imp.NAME(0).getText();
                String alias;

                if (imp.NAME().size() > 1) {
                    alias = imp.NAME(1).getText();
                } else {
                    alias = name;
                }

                importables.put(name, alias);
            }
        } else {
            // @TODO fix me (globbed imports)
            importables.put("*", "*");
        }

        String from = null;

        if (ctx.import_from() != null) {
            for (ParseTree child : ctx.import_from().children) {
                if (child.getClass() == Python3Parser.Dotted_nameContext.class) {
                    from = ctx.import_from().dotted_name().getText();
                    break;
                } else if (child.getClass() == TerminalNodeImpl.class) {
                    if (!child.getText().equals("from")) {
                        from = child.getText();
                        break;
                    }
                }
            }
        }

        Import imp = new Import(importables, from);
        imports.add(imp);

        return null;
    }
}
