package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class AndExpressionVisitor extends StatementVisitor {
    public AndExpressionVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitAnd_expr(Python3Parser.And_exprContext ctx) {
        if (ctx.getChildCount() == 1) {
            visitChildren(ctx);
        } else {
            visit(ctx.children.get(0));
            context.writeRuby(" & ");
            visit(ctx.children.get(2));
        }

        return null;
    }
}
