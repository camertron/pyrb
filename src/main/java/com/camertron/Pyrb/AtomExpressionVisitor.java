package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Lexer;
import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AtomExpressionVisitor extends StatementVisitor {
    private static final Pattern STRING_RE = Pattern.compile("(?<!\\\\)(\\\\x[a-fA-F0-9]{2})+");

    enum ForCompType {
        NONE, LIST, DICT_OR_SET
    }

    public AtomExpressionVisitor(BaseContext context) {
        super(context);
    }

    public Void visitAtom_expr(Python3Parser.Atom_exprContext ctx) {
        switch (forCompType(ctx.atom())) {
            case LIST:
                // let the for statement visitor handle this
                return visit(ctx.atom().testlist_comp().comp_for());

            case DICT_OR_SET:
                return visit(ctx.atom().dictorsetmaker().comp_for());
        }

        if (containsTuple(ctx.atom())) {
            context.writeRuby("[");
            visit(ctx.atom().testlist_comp());
            context.writeRuby("]");
            return null;
        }

        visit(ctx.atom());
        emitTrailerList(ctx);

        return null;
    }

    @Override
    public Void visitAtom(Python3Parser.AtomContext ctx) {
        for (int i = 0; i < ctx.children.size(); i ++) {
            ParseTree child = ctx.children.get(i);

            if (child.getClass() == Python3Parser.Testlist_compContext.class) {
                visit(child);
            } else if (child.getClass() == Python3Parser.DictorsetmakerContext.class) {
                visit(child);
            } else if (child.getClass() == TerminalNodeImpl.class && child != ctx.NAME() && ctx.NAME() != null) {
                // this handles things like enclosing parens that are otherwise not part
                // of the parse tree
                context.writeRuby(child.getText());
            } else if (child == ctx.NUMBER()) {
                emitNumber(ctx.NUMBER().getText());
            } else if (ctx.STRING() != null && ctx.STRING().contains(child)) {
                ArrayList<ParseTree> stringNodes = new ArrayList<ParseTree>();
                int n = i;

                // Collect all sequential string literals into an array so we know how many there are
                // and therefore whether or not to surround them with parens. We do this to support
                // strings that are implicitly concatenated, i.e. "foo" "bar", which is legal syntax
                // in both Python and Ruby.
                while (n < ctx.children.size() && ctx.STRING().contains(ctx.children.get(n))) {
                    stringNodes.add(ctx.children.get(n));
                    n ++;
                }

                if (stringNodes.size() > 1) {
                    context.writeRuby("(");
                }

                for (int c = 0; c < stringNodes.size(); c ++) {
                    if (c > 0) {
                        context.writeRuby(" + ");
                    }

                    String text = stringNodes.get(c).getText();

                    if (text.startsWith("f")) {
                        emitFString(text);
                    } else {
                        emitString(text);
                    }
                }

                if (stringNodes.size() > 1) {
                    context.writeRuby(")");
                }

                i += stringNodes.size() - 1;
            } else {
                if (!shouldEmitTerminal(ctx, child)) {
                    continue;
                }

                // this happens for empty dicts
                if (child.getText().equals("{")) {
                    if (ctx.dictorsetmaker() == null) {
                        context.writeRuby("Pyrb::Dict.new");
                    }

                    continue;
                } else if (child.getText().equals("}")) {
                    continue;
                }

                Symbol sym = getSymbolFor(child.getText());

                if (sym == null) {
                    context.writeRuby(child.getText());
                } else {
                    context.writeRuby(sym.getRubyExpression());
                }
            }
        }

        return null;
    }

    private boolean shouldEmitTerminal(Python3Parser.AtomContext ctx, ParseTree terminal) {
        String text = terminal.getText();

        if (text.equals("[") || text.equals("]")) {
            // This indicates square brackets around a list comprehension. Since the ruby
            // implementation calls #map, we need to skip emitting square brackets here.
            return forCompType(ctx) == ForCompType.NONE;
        }

        if (text.equals("{") || text.equals("}")) {
            // for loops can be surrounded by {}, which means the loop constructs a dict or set
            return forCompType(ctx) == ForCompType.NONE;
        }

        return true;
    }

    private ForCompType forCompType(Python3Parser.AtomContext ctx) {
        if (ctx.testlist_comp() != null && ctx.testlist_comp().comp_for() != null) {
            return ForCompType.LIST;
        } else if (ctx.dictorsetmaker() != null && ctx.dictorsetmaker().comp_for() != null) {
            return ForCompType.DICT_OR_SET;
        } else {
            return ForCompType.NONE;
        }
    }

    private boolean containsTuple(Python3Parser.AtomContext ctx) {
        // check all children here to account for the case when the python programmer tries to
        // create a tuple with a single element, eg. (1,) - checking all children counts the
        // trailing comma
        return ctx.testlist_comp() != null && ctx.testlist_comp().children.size() > 1;
    }

    private Symbol getSymbolFor(String pythonStr) {
        if (context.getSymbolTable().contains(pythonStr)) {
            return context.getSymbolTable().getSymbol(pythonStr);
        }

        if (Python.isIdentifier(pythonStr)) {
            if (context.getCurrentScope().getType() == ScopeType.CLASS) {
                context.getSymbolTable().addSymbol(pythonStr, "klass.attrs['" + Ruby.toIdentifier(pythonStr) + "']");
            } else if (context.getCurrentScope().getType() == ScopeType.FILE) {
                context.getSymbolTable().addSymbol(pythonStr, "exports['" + Ruby.toIdentifier(pythonStr) + "']");
            } else {
                context.getSymbolTable().addSymbol(pythonStr, Ruby.toIdentifier(pythonStr));
            }
        }

        return context.getSymbolTable().getSymbol(pythonStr);
    }

    private void emitFunctionCall(String funcName, Python3Parser.ArglistContext argumentList) {
        context.writeRuby(".fcall('");
        context.writeRuby(funcName);
        context.writeRuby("'");

        if (argumentList != null && argumentList.argument().size() > 0) {
            context.writeRuby(", ");
            emitArgumentList(argumentList);
        }

        context.writeRuby(")");
    }

    private void emitArgumentList(Python3Parser.ArglistContext argumentList) {
        emitArgumentList(argumentList, new ArrayList<String>());
    }

    private void emitArgumentList(Python3Parser.ArglistContext argumentList, ArrayList<String> extraPositional) {
        if (argumentList == null && extraPositional.isEmpty()) {
            return;
        }

        boolean argsOpen = true;
        boolean kwargsOpen = false;
        boolean emittedExtras = false;

        context.writeRuby("[");

        if (argumentList != null) {
            for (int i = 0; i < argumentList.argument().size(); i++) {
                Python3Parser.ArgumentContext arg = argumentList.argument(i);

                // Splat!
                boolean isSplat = arg.children.get(0).getText().equals("*");
                boolean isKwsplat = arg.children.get(0).getText().equals("**");

                if (isSplat) {
                    if (i > 0) {
                        context.writeRuby(", ");
                    }

                    context.writeRuby("*Pyrb.splat(");
                } else if (isKwsplat) {
                    context.writeRuby("], { **(");
                    argsOpen = false;
                    kwargsOpen = true;
                } else {
                    if (i > 0) {
                        context.writeRuby(", ");
                    }
                }

                // arguments with more than one test component must represent a keyword argument,
                // so turn them into ruby keyword args
                if (arg.test().size() > 1) {
                    for (int ex = 0; ex < extraPositional.size(); ex++) {
                        if (ex > 0 && i > 0) {
                            context.writeRuby(", ");
                        }

                        context.writeRuby(extraPositional.get(ex));
                    }

                    emittedExtras = true;

                    if (argsOpen && !kwargsOpen) {
                        context.writeRuby("], { ");
                        argsOpen = false;
                        kwargsOpen = true;
                    }

                    visit(arg.test(0));
                    context.writeRuby(": ");
                    visit(arg.test(1));
                } else {
                    // if the argument is a list comprehension, only visit the
                    // comprehension and not the whole argument (avoids printing
                    // the expression value twice; the ForStatementVisitor reaches
                    // up into the parent to access it).
                    if (arg.comp_for() == null) {
                        visit(arg);
                    } else {
                        visit(arg.comp_for());
                    }
                }

                if (isSplat || isKwsplat) {
                    context.writeRuby(")");
                }
            }
        }

        if (!emittedExtras) {
            for (int ex = 0; ex < extraPositional.size(); ex++) {
                if (ex > 0 || argumentList != null) {
                    context.writeRuby(", ");
                }

                context.writeRuby(extraPositional.get(ex));
            }
        }

        if (argsOpen) {
            context.writeRuby("]");
        } else if (kwargsOpen) {
            context.writeRuby(" }");
        }
    }

    public void emitTrailerList(Python3Parser.Atom_exprContext atomExpr) {
        emitTrailerList(atomExpr, new TrailerShapeBuilder(atomExpr).all());
    }

    public void emitTrailerList(Python3Parser.Atom_exprContext atomExpr, TrailerShape shape) {
        String closePropAccess = null;

        for (int trailerIdx = 0; trailerIdx < atomExpr.trailer().size(); trailerIdx ++) {
            if (!shape.containsTrailer(trailerIdx)) {
                continue;
            }

            Python3Parser.TrailerContext trailer = atomExpr.trailer(trailerIdx);

            if (closePropAccess != null) {
                context.writeRuby(closePropAccess);
                closePropAccess = null;
            }

            if (trailer.children.get(0).getText().equals("(")) {
                context.writeRuby(".call(");

                if (atomExpr.getText().equals("super") && trailerIdx == 0) {
                    ArrayList<String> extraPositional = new ArrayList<String>();

                    if (trailer.arglist() == null) {
                        extraPositional.add(context.getCurrentScope().getClassRef().getRubyExpression());
                        extraPositional.add(context.getCurrentScope().getSelfRef().getRubyExpression());
                    } else if (trailer.arglist().argument().size() == 1) {
                        extraPositional.add(context.getCurrentScope().getSelfRef().getRubyExpression());
                    }

                    emitArgumentList(trailer.arglist(), extraPositional);
                } else {
                    if (trailer.arglist() != null) {
                        emitArgumentList(trailer.arglist());
                    }
                }

                context.writeRuby(")");

                continue;
            }

            if (atomExpr.trailer().size() > trailerIdx + 1 && atomExpr.trailer(trailerIdx + 1).children.get(0).getText().equals("(")) {
                // this is a function call
                String funcName = trailer.children.get(1).getText();
                Python3Parser.ArglistContext argumentList = atomExpr.trailer(trailerIdx + 1).arglist();
                emitFunctionCall(funcName, argumentList);
                trailerIdx ++;
                continue;
            }

            for (int childIdx = 0; childIdx < trailer.children.size(); childIdx ++) {
                if (!shape.containsChild(trailerIdx, childIdx)) {
                    continue;
                }

                ParseTree child = trailer.children.get(childIdx);

                if (child.getClass() == Python3Parser.SubscriptlistContext.class) {
                    emitSubscriptList((Python3Parser.SubscriptlistContext)child);
                } else if (child.getClass() == TerminalNodeImpl.class) {
                    ExpressionChild expr = findChildInParentExpr(child);

                    if (child.getText().equals(".")) {
                        if (expr != null && expr.isAssignment() && expr.getSide() == ExpressionChild.Side.LEFT) {
                            context.writeRuby(".attrs['");
                            closePropAccess = "']";
                        } else {
                            context.writeRuby(".attr('");
                            closePropAccess = "')";
                        }
                    } else {
                        context.writeRuby(child.getText());
                    }
                }
            }
        }

        if (closePropAccess != null) {
            context.writeRuby(closePropAccess);
        }
    }

    public void emitSubscriptList(Python3Parser.SubscriptlistContext subscriptList) {
        for (Python3Parser.SubscriptContext subscript : subscriptList.subscript()) {
            if (subscript.test().size() == subscript.children.size()) {
                // indicates we've encountered a basic 'ol subscript, eg. foo[0]
                visitChildren(subscript);
            } else {
                // indicates we've encountered a range-based subscript like foo[0:], foo[0:2], or foo[0:1:2]
                // the number if test()s and their positions in the list of children will tell us which kind

                Python3Parser.TestContext first = null;
                Python3Parser.TestContext second = null;
                Python3Parser.TestContext step = null;

                if (subscript.test().size() == 1) {
                    if (subscript.children.get(0) == subscript.test(0)) {
                        first = subscript.test(0);
                    } else {
                        second = subscript.test(0);
                    }
                } else if (subscript.test().size() == 2) {
                    first = subscript.test(0);
                    second = subscript.test(1);
                }

                // sliceop is always in the third position and indicates the step value
                if (subscript.sliceop() != null) {
                    step = subscript.sliceop().test();
                }

                // write the thing out
                if (step == null) {
                    emitNativeRange(first, second);
                } else {
                    emitAugmentedRange(first, second, step);
                }
            }
        }
    }

    private void emitNativeRange(Python3Parser.TestContext first, Python3Parser.TestContext second) {
        if (first == null) {
            context.writeRuby("0");
        } else {
            visit(first);
        }

        if (second == null) {
            context.writeRuby("..-1");
        } else {
            // ranges in python are always exclusive
            context.writeRuby("...");
            visit(second);
        }
    }

    private void emitAugmentedRange(Python3Parser.TestContext first, Python3Parser.TestContext second, Python3Parser.TestContext step) {
        context.writeRuby("Pyrb::Range.new([");

        if (first == null) {
            context.writeRuby("nil");
        } else {
            visit(first);
        }

        context.writeRuby(", ");

        if (second == null) {
            context.writeRuby("nil");
        } else {
            visit(second);
        }

        if (step != null) {
            context.writeRuby(", ");
            visit(step);
        }

        context.writeRuby("])");
    }

    private void emitString(String orig) {
        boolean isRaw = false;
        boolean isBytes = false;
        boolean tripleQuoted = false;
        boolean singleQuoted = false;
        String encoding = "UTF-8";
        String packFormat = "U";

        for (int i = 0; i < orig.length(); i ++) {
            char current = orig.charAt(i);

            if (current == 'r' || current == 'R') {
                isRaw = true;
            } else if (current == 'b' || current == 'B') {
                isBytes = true;
                encoding = "ASCII-8BIT";
                packFormat = "C";
            } else {
                orig = orig.substring(i);
                break;
            }
        }

        if (orig.startsWith("\"\"\"") || orig.startsWith("'''")) {
            tripleQuoted = true;
            orig = orig.substring(3, orig.length() - 3);
        } else {
            if (orig.startsWith("'")) {
                singleQuoted = true;
            }

            orig = orig.substring(1, orig.length() - 1);
        }

        if (isBytes) {
            context.writeRuby("Pyrb::Bytes.call([");
        }

        if (isRaw) {
            if (orig.contains("\n")) {
                emitDocstring(orig);
            } else {
                context.writeRuby(Ruby.wrapRawString(orig));
            }
        } else if (tripleQuoted && orig.contains("\n")) {
            emitDocstring(orig);
        } else {
            // Python strings can be surrounded by either single or double quotes - both
            // work the same way. Ruby on the other hand only allows interpolation and
            // most escape sequences inside double-quoted strings. To account for this,
            // we emit only double-quoted strings, and escape all single quotes inside them.
            String str = Ruby.escapeInterpolationChars(orig);

            if (singleQuoted || tripleQuoted) {
                str = Ruby.escapeDoubleQuotedString(str);
            }

            // The remainder of this method body is concerned with identifying runs of sequences
            // in the source string that use the \x notation to specify literal bytes (or, as
            // we'll learn later, Unicode codepoints).
            //
            // For example, because Python strings are encoded in UTF-8 by definition, the "\xE9"
            // string literal is equivalent to the string "é". Python interprets and displays the
            // byte sequences as Unicode codepoints. Unfortunately (or fortunately, depending on
            // how crazy you think that implicit conversion is), Ruby doesn't work the same way.
            // The same sequence in Ruby, "\xE9", is interpreted literally as a single-byte string.
            // No automagical codepoint conversions occur, and we're left with a string that
            // contains an invalid byte sequence (the correct byte sequence is 0xC3, 0xA9). So
            // basically what I'm saying is that Python is ridiculous. But you already knew that.
            //
            // To combat the problem, we need to make sure to specifically handle byte sequences
            // in Python strings. The code below turns a string like "abc \xE9 def" into:
            //
            // ("abc " + [0xE9].pack('U*') + " def")
            //
            // (NOTE: we should be able to rely on the #encoding comment at the top of each converted
            // file to encode all string literals as UTF-8 by default)
            //
            Matcher m = STRING_RE.matcher(str);
            int lastEnd = 0;
            int counter = 0;

            if (!m.find()) {
                context.writeRuby("\"");
                context.writeRuby(str);
                context.writeRuby("\"");
            } else {
                if (encoding.equals("UTF-8")) {
                    context.writeRuby("(");
                } else {
                    context.writeRuby("String.new(");
                }

                do {
                    if (counter > 0) {
                        context.writeRuby(" + ");
                    }

                    int start = m.start();

                    if (lastEnd != start) {
                        context.writeRuby("\"");
                        context.writeRuby(str.substring(lastEnd, start));
                        context.writeRuby("\" + ");
                    }

                    context.writeRuby("[");

                    int hexCounter = 0;

                    for (String b : m.group().split("\\\\x")) {
                        if (b.isEmpty()) {
                            continue;
                        }

                        if (hexCounter > 0) {
                            context.writeRuby(", ");
                        }

                        hexCounter++;

                        context.writeRuby("0x");
                        context.writeRuby(b);
                    }

                    context.writeRuby("].pack('" + packFormat + "*')");

                    lastEnd = m.end();
                    counter++;
                } while (m.find());

                if (lastEnd != str.length()) {
                    if (counter > 0) {
                        context.writeRuby(" + ");
                    }

                    context.writeRuby("\"");
                    context.writeRuby(str.substring(lastEnd));
                    context.writeRuby("\"");
                }

                if (!encoding.equals("UTF-8")) {
                    context.writeRuby(", encoding: '" + encoding + "'");
                }

                context.writeRuby(")");
            }
        }

        if (isBytes) {
            context.writeRuby("])");
        }
    }

    private void emitFString(String str) {
        // removeChild f and quotes
        str = str.substring(2, str.length() - 1);

        Pattern pattern = Pattern.compile("\\{[^}]*\\}");
        Matcher matcher = pattern.matcher(str);
        int lastEnd = 0;

        context.writeRuby("\"");

        while (matcher.find()) {
            context.writeRuby(str.substring(lastEnd, matcher.start(0)));

            // thanks for not doing this for me, python parser
            context.writeRuby("#{");
            String fragment = matcher.group(0).substring(1, matcher.group(0).length() - 1);
            String codeFragment = fragment;

            // check to see if the expression ends with a suffix like !r (print the expression's
            // __repr__() value) or !s (print the expression's __str__() value)
            if (fragment.matches(".*!\\w\\z")) {
                codeFragment = fragment.substring(0, fragment.length() - 2);
            }

            // code fragments have to end in a newline because otherwise PythonParser will
            // spit out a warning that it encountered an unexpected <EOF>
            Python3Lexer lexer = new Python3Lexer(CharStreams.fromString(codeFragment + "\n"));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Python3Parser parser = new Python3Parser(tokens);
            Python3Parser.File_inputContext file = parser.file_input();
            BaseContext fragmentContext = new BaseContext(context.scopeStack);
            new StatementVisitor(fragmentContext).visit(file);
            context.writeRuby(fragmentContext.getRubyOutput().trim());

            if (fragment.endsWith("!r")) {
                // !r after a variable name in a format string means print that variable's
                // __repr__() value, analogous to Ruby's #inspect
                context.writeRuby(".fcall('__repr__')");
            } else if (fragment.endsWith("!s")) {
                // !s after a variable name in a format string means print that variable's
                // __str__() value, analogous to Ruby's #to_s
                context.writeRuby(".fcall('__str__')");
            }

            context.writeRuby("}");

            lastEnd = matcher.end(0);
        }

        if (lastEnd < str.length()) {
            context.writeRuby(str.substring(lastEnd));
        }

        context.writeRuby("\"");
    }

    private void emitDocstring(String str) {
        String[] lines = str.split("\\r?\\n");

        int minLs = -1;
        int firstLine = -1;

        // find first non-empty line that isn't the first line
        for (int i = 1; i < lines.length; i ++) {
            if (!lines[i].trim().isEmpty()) {
                firstLine = i;
                break;
            }
        }

        if (firstLine == -1) {
            firstLine = 0;
        }

        for (int i = firstLine; i < lines.length; i ++) {
            String line = lines[i];

            if (line.trim().isEmpty()) {
                continue;
            }

            int curMin = 0;

            for (int c = 0; c < line.length(); c ++) {
                if (line.charAt(c) == ' ') {
                    curMin ++;
                } else {
                    break;
                }
            }

            if (minLs == -1 || curMin < minLs) {
                minLs = curMin;
            }

            // no need to keep going
            if (minLs == 0) {
                break;
            }
        }

        for (int i = 0; i < lines.length; i ++) {
            if (!lines[i].startsWith(" ")) {
                continue;
            }

            if (lines[i].length() >= minLs) {
                lines[i] = lines[i].substring(minLs);
            } else {
                lines[i] = "";
            }
        }

        context.writeRuby("<<~__DOC__");
        context.writeHeredoc(String.join("\n", lines).trim());
        context.writeHeredoc("\n__DOC__\n");
    }

    private void emitNumber(String num) {
        char last = num.charAt(num.length() - 1);

        if ((last >= '0' && last <= '9') || num.startsWith("0x")) {
            context.writeRuby(num);
            return;
        }

        String rest = num.substring(0, num.length() - 1);

        switch (last) {
            case 'J':
            case 'j':
                // Complex number with a zero imaginary part
                context.writeRuby("Complex(");
                context.writeRuby(rest);
                context.writeRuby(")");
                break;

            case 'L':
            case 'l':
                // Long int. No equivalent in ruby, but regular ruby integers can handle
                // numbers of long magnitude, so no need to do anything special here.
                context.writeRuby(rest);

            default:
                throw new RuntimeException("Unexpected integer suffix '" + last + "'");
        }
    }
}
