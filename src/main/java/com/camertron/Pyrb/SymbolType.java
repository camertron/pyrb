package com.camertron.Pyrb;

enum SymbolType { LOCAL, BLOCK_LOCAL, GLOBAL, IMPORT, BUILTIN, CLASS, DEF, ARG }