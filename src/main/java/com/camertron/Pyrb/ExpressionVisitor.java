package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class ExpressionVisitor extends StatementVisitor {
    public ExpressionVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitExpr_stmt(Python3Parser.Expr_stmtContext ctx) {
        int lhsLength = 0;
        boolean deconstruct = false;
        int openGroups = 0;

        for (int i = 0; i < ctx.children.size(); i += 2) {
            ParseTree operand = ctx.children.get(i);
            String operator = null;

            if (i < ctx.children.size() - 1) {
                operator = ctx.children.get(i + 1).getText();
            }

            if (i == ctx.children.size() - 1) {
                // we have reached the right-hand side of the expression
                if (lhsLength > 1) {
                    deconstruct = true;
                    context.writeRuby("Pyrb.deconstruct(" + lhsLength + ", ");
                }
            } else {
                int termCount = 0;

                // count the number of lhs terms
                for (int c = 0; c < operand.getChildCount(); c ++) {
                    if (operand.getChild(c).getClass() != TerminalNodeImpl.class) {
                        termCount ++;
                    }
                }

                // we're still on the left-hand side of the expression
                if (termCount > lhsLength) {
                    // trying to get the max number of lhs assignments
                    lhsLength = termCount;
                }

                // since we're still on the left-hand side, wrap nested deconstructed assignments in
                // parens, since ruby can't handle stuff like a = b, c = ['e', 'f'] without them
                if (i >= 1 && lhsLength > 1) {
                    context.writeRuby("(");
                    openGroups += 1;
                }
            }

            if (operator != null && operator.endsWith("=")) {
                for (int oc = 0; oc < operand.getChildCount(); oc ++) {
                    ParseTree operandChild = operand.getChild(oc);

                    if (operandChild.getClass() == TerminalNodeImpl.class) {
                        context.writeRuby(Ruby.pad(operandChild.getText()));
                    } else {
                        String ident = operandChild.getText();

                        if (Python.isIdentifier(ident)) {
                            Symbol sym = context.getSymbolTable().getSymbol(ident);

                            if (sym == null || sym.getType() != SymbolType.GLOBAL) {
                                if (context.getCurrentScope().getType() == ScopeType.CLASS) {
                                    // assignment to a variable inside a class definition
                                    String rubyExpression = "klass.attrs['" + ident + "']";
                                    context.writeRuby(rubyExpression);
                                    context.getSymbolTable().addSymbol(ident, rubyExpression);
                                } else if (context.getCurrentScope().getType() == ScopeType.FILE) {
                                    String rubyExpression = "exports['" + ident + "']";
                                    context.writeRuby(rubyExpression);
                                    context.getSymbolTable().addSymbol(ident, rubyExpression);
                                } else {
                                    // assignment to local variable (allows shadowing function with same name)
                                    context.writeRuby(Ruby.toIdentifier(ident));
                                    context.getSymbolTable().addSymbol(ident, ident);
                                }
                            } else {
                                context.writeRuby(sym.getRubyExpression());
                            }
                        } else if (ident.startsWith("(")) {
                            // this is probably a tuple since it's being assigned to, so removeChild the parens
                            // and visit everything in between
                            Python3Parser.AtomContext atom = findNextAtomExpr(operandChild).atom();

                            for (int tc = 1; tc < atom.children.size() - 1; tc ++) {
                                visit(atom.children.get(tc));
                            }
                        } else {
                            visit(operandChild);
                        }
                    }
                }
            } else {
                visit(operand);
            }

            if (operator != null) {
                context.writeRuby(Ruby.pad(operator));
            }
        }

        if (deconstruct) {
            context.writeRuby(")");
        }

        for (int i = 0; i < openGroups; i ++) {
            context.writeRuby(")");
        }

        context.writeRubyLine();

        return null;
    }

    @Override
    public Void visitExpr(Python3Parser.ExprContext ctx) {
        for (ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                context.writeRuby(Ruby.pad(child.getText()));
            } else {
                visit(child);
            }
        }

        return null;
    }
}
