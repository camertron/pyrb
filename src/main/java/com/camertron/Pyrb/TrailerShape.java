package com.camertron.Pyrb;

import java.util.HashMap;
import java.util.HashSet;

public class TrailerShape {
    private HashMap<Integer, HashSet<Integer>> shape;

    public TrailerShape() {
        this(new HashMap<Integer, HashSet<Integer>>());
    }

    public TrailerShape(HashMap<Integer, HashSet<Integer>> shape) {
        this.shape = shape;
    }

    public void add(int trailerIdx, int childIdx) {
        shape.computeIfAbsent(trailerIdx, k -> new HashSet<Integer>());
        shape.get(trailerIdx).add(childIdx);
    }

    public void removeTrailer(int trailerIdx) {
        shape.remove(trailerIdx);
    }

    public void removeChild(int trailerIdx, int childIdx) {
        if (!shape.containsKey(trailerIdx)) {
            return;
        }

        shape.get(trailerIdx).remove(childIdx);
    }

    public boolean containsTrailer(int trailerIdx) {
        return shape.containsKey(trailerIdx);
    }

    public boolean containsChild(int trailerIdx, int childIdx) {
        if (!shape.containsKey(trailerIdx)) {
            return false;
        }

        return shape.get(trailerIdx).contains(childIdx);
    }
}
