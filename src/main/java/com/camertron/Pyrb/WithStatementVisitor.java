package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class WithStatementVisitor extends StatementVisitor {
    public WithStatementVisitor(BaseContext context) {
        super(context);
    }

    public Void visitWith_stmt(Python3Parser.With_stmtContext ctx) {
        // I have no idea why there's more than one of these
        Python3Parser.With_itemContext with = ctx.with_item(0);
        String asVar = null;

        // the "as" variable gets added to the local scope (because with statements
        // don't introduce their own scope)
        if (with.expr() != null) {
            asVar = with.expr().getText();
            context.getSymbolTable().addSymbol(asVar, asVar);
            asVar = context.getSymbolTable().getSymbol(asVar).getRubyExpression();
            context.writeRuby(asVar);
            context.writeRuby(" = ");
        }

        context.writeRuby("Pyrb.with.call([");
        visit(with.test());
        context.writeRuby("]) do");

        if (with.expr() != null) {
            context.writeRuby(" |");
            context.writeRuby(asVar);
            context.writeRuby("|");
        }

        context.writeRubyLine();

        context.enterScope(ScopeType.BLOCK);
        context = BlockContext.enter(context);
        visit(ctx.suite());
        context = ((BlockContext)context).exit();
        context.exitScope();
        context.writeRubyLine("end");

        return null;
    }
}
