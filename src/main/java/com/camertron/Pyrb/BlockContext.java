package com.camertron.Pyrb;

import java.util.ArrayList;

// Designed to temporarily enter the context referenced by a visitor in order to capture
// the contents of a block and move any local variables declared inside the block to the
// outer scope. This is because Python for loops, with statements, etc, don't introduce
// their own scope. Local variables assigned "inside" them are available to the containing
// scope, eg. the rest of the code inside a method definition.
//
// For example, consider this Python snippet:
//
//   def foo():
//     print(bar)
//
//   for x in [1, 2, 3]:
//     bar = x
//     foo()
//
// When executed, the code above will print "1", "2", then "3". Because the for loop doesn't
// introduce a new scope, the local variable bar is available when foo() is called. Local
// variables declared inside Ruby blocks are not available to the outer scope since Ruby
// blocks introduce their own scope. To work around this, Pyrb uses the BlockContext class
// below to evaluate the function, class, or block in a separate context. The separate context
// provides an entirely separate output buffer that only the contents of the block are written
// to. The symbol table keeps track of local variables (called block locals in Pyrb parlance)
// and initializes them to nil immediately before copying the BlockContext's output buffer into
// the original output buffer. Block locals are therefore made available to the entire scope.
//
// The corresponding Ruby code for the Python snippet above might be:
//
// bar = nil
//
// foo = Pyrb.defn do
//   Pyrb.print.call([bar])
// end
//
// [1, 2, 3].each do |x|
//   bar = x
//   foo.call
// end
//
public class BlockContext extends BaseContext {
    private BaseContext originalContext;
    private Scope originalScope;

    public static BlockContext enter(BaseContext originalContext) {
        return new BlockContext(originalContext, originalContext.getCurrentScope());
    }

    public BlockContext(BaseContext originalContext, Scope originalScope) {
        super(originalContext.scopeStack);
        this.originalContext = originalContext;
        this.originalScope = originalScope;
    }

    public BaseContext exit() {
        ArrayList<Symbol> locals = originalScope.getSymbolTable().getBlockLocals();
        Scope innermost = originalScope.getInnermostLocalScope();

        if (originalScope != innermost) {
            // Move block locals from this scope to the innermost scope-introducing thing,
            // i.e. the closest method, class, or file definition. Although nothing will
            // use them (the code inside the block has already been visited at this point),
            // it prevents the code below from initializing them to nil, which should be
            // done at the aforementioned innermost local scope. In other words, hand
            // these block locals up the tree until it's time to initialize them.
            for (Symbol local : locals) {
                originalScope.getSymbolTable().removeSymbol(local.getPythonName());
                innermost.getSymbolTable().addSymbol(local);
            }
        }

        locals = originalScope.getSymbolTable().getBlockLocals();

        // Set block local variables (i.e. variables declared/set inside block contexts) to
        // nil at the top of the enclosing scope so they can be referenced by the entire
        // contents of the scope.
        if (!locals.isEmpty()) {
            String[] rbLocals = new String[locals.size()];
            int counter = 0;

            for (Symbol local : locals) {
                rbLocals[counter] = local.getRubyExpression();
                counter++;
            }

            originalContext.writeRuby(Utils.join(rbLocals, ", "));
            originalContext.writeRubyLine(" = nil\n");
        }

        originalContext.writeRubyLine(getRubyOutput().trim());

        return originalContext;
    }
}
