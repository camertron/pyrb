package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public class PropertyDecorator implements IDecorator {
    private Python3Parser.FuncdefContext funcDef;
    private String contextRef;

    public PropertyDecorator(Python3Parser.FuncdefContext funcDef, String contextRef) {
        this.funcDef = funcDef;
        this.contextRef = contextRef;
    }

    public void applyTo(BaseContext context) {
        context.writeRuby(contextRef);
        context.writeRuby(".properties['");
        context.writeRuby(funcDef.NAME().getText());
        context.writeRuby("'] = Pyrb::Property.new([");
        context.enterScope(ScopeType.INVOCATION);
        new FuncDefVisitor(context).visit(funcDef);
        context.exitScope();
        context.writeRubyLine("])\n");
    }

    public String getContextRef() {
        return contextRef;
    }

    public Python3Parser.FuncdefContext getFuncDef() {
        return funcDef;
    }

    public IDecorator getSubDecorator(ArrayList<String> subNames, ParseTree ctx) {
        if (subNames.size() == 1 && subNames.get(0).equals("setter")) {
            return new SetterDecorator(this, ctx);
        }

        throw new RuntimeException("Sub-decorator '" + subNames.get(0) + "' isn't supported by the property decorator");
    }
}
