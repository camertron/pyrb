package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class AssertVisitor extends StatementVisitor {
    public AssertVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitAssert_stmt(Python3Parser.Assert_stmtContext ctx) {
        context.writeRuby("Pyrb.assert(");

        for (int i = 0; i < ctx.test().size(); i ++) {
            if (i > 0) {
                context.writeRuby(", ");
            }

            visit(ctx.test(i));
        }

        context.writeRubyLine(")");

        return null;
    }
}
