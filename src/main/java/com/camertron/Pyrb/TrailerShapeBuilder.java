package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

public class TrailerShapeBuilder {
    private Python3Parser.Atom_exprContext atomExpr;

    public TrailerShapeBuilder(Python3Parser.Atom_exprContext atomExpr) {
        this.atomExpr = atomExpr;
    }

    public TrailerShape all() {
        TrailerShape shape = new TrailerShape();

        for (int i = 0; i < atomExpr.trailer().size(); i ++) {
            Python3Parser.TrailerContext trailer = atomExpr.trailer(i);

            for (int j = 0; j < trailer.children.size(); j ++) {
                shape.add(i, j);
            }
        }

        return shape;
    }

    public TrailerShape lastWithoutSubscript() {
        TrailerShape shape = new TrailerShape();

        int lastTrailerIdx = atomExpr.trailer().size() - 1;
        Python3Parser.TrailerContext lastTrailer = atomExpr.trailer(lastTrailerIdx);

        for (int j = 0; j < lastTrailer.children.size(); j ++) {
            shape.add(lastTrailerIdx, j);
        }

        ParseTree firstChild = lastTrailer.children.get(0);

        if (firstChild.getText().equals("[")) {
            shape.removeChild(lastTrailerIdx, lastTrailer.children.size() - 1);
        }

        // removes either an opening bracket or a leading .
        shape.removeChild(lastTrailerIdx, 0);

        return shape;
    }

    public TrailerShape allButLastTrailer() {
        TrailerShape shape = all();
        shape.removeTrailer(atomExpr.trailer().size() - 1);
        return shape;
    }
}
