package com.camertron.Pyrb;

import java.util.HashMap;
import java.util.Map;

public class Import {
    private HashMap<String, String> importables;
    private HashMap<String, String> reassignments;
    private String from;

    public Import(HashMap<String, String> importables, String from) {
        this.importables = importables;
        this.from = from;
        this.reassignments = new HashMap<String, String>();
    }

    public Import(HashMap<String, String> importables, String from, HashMap<String, String> reassignments) {
        this.importables = importables;
        this.from = from;
        this.reassignments = reassignments;
    }

    public Import applyTo(BaseContext context) {
        HashMap<String, String> newImportables = new HashMap<String, String>();
        HashMap<String, String> reassignments = new HashMap<String, String>();

        for (HashMap.Entry<String, String> importable : importables.entrySet()) {
            String var = importable.getKey();
            String alias = importable.getValue();

            // Do this crap because python treats a single underscore as a variable name
            // differently than ruby does. For example in Python:
            //
            // >>> _, = ['abc']
            // >>> _
            // "abc"
            //
            // Whereas in Ruby:
            //
            // irb> _, = ['abc']
            // irb> _
            // ["abc"]
            //
            // BUT also in Ruby:
            //
            // irb> a, = ['abc']
            // irb> a
            // "abc"
            //
            // Apparently ruby thinks "_" is special. Damn it.
            //
            if (alias.equals("_")) {
                String tmp = Ruby.getUniqueVariableName("_tmp_", context.getCurrentScope().getSymbolTable());
                reassignments.put(tmp, alias);
                newImportables.put(var, tmp);
                context.getSymbolTable().addSymbol(alias, alias, SymbolType.IMPORT);
            } else if (Ruby.isConstant(alias)) {
                switch (context.getCurrentScope().getType()) {
                    case FILE:
                    case CLASS:
                        break;

                    default:
                        String lowercasedAlias = alias.substring(0, 1).toLowerCase() + alias.substring(1);
                        alias = Ruby.getUniqueVariableName(lowercasedAlias, context.getCurrentScope().getSymbolTable());
                        break;
                }

                context.getSymbolTable().addSymbol(var, alias, SymbolType.IMPORT);
                newImportables.put(var, alias);
            } else {
                newImportables.put(var, alias);
                context.getSymbolTable().addSymbol(alias, alias, SymbolType.IMPORT);
            }
        }

        return new Import(newImportables, from, reassignments);
    }

    public String toRuby() {
        StringBuilder result = new StringBuilder();
        result.append(Utils.join(importables.values(), ", "));

        if (importables.size() == 1) {
            result.append(", _");
        }

        result.append(" = import(");
        int counter = 0;

        for (Map.Entry<String, String> importable : importables.entrySet()) {
            String module = importable.getKey();

            if (counter > 0) {
                result.append(", ");
            }

            result.append("'");
            result.append(module);
            result.append("'");

            counter ++;
        }

        if (from != null) {
            result.append(", from: '");
            result.append(from);
            result.append("'");
        }

        result.append(")");

        for (HashMap.Entry<String, String> reassignment : reassignments.entrySet()) {
            result.append("\n");
            result.append(reassignment.getValue());
            result.append(" = ");
            result.append(reassignment.getKey());
        }

        return result.toString();
    }
}
