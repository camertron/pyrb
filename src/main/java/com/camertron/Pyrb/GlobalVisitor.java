package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.TerminalNode;

public class GlobalVisitor extends StatementVisitor {
    public GlobalVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitGlobal_stmt(Python3Parser.Global_stmtContext ctx) {
        for (TerminalNode name : ctx.NAME()) {
            Symbol sym = context.getCurrentScope().getSymbolTable().getGlobalSymbol(name.getText());

            if (sym == null) {
                throw new RuntimeException("Could not find symbol '" + name.getText() + "'");
            }

            sym.setType(SymbolType.GLOBAL);
        }

        return null;
    }
}
