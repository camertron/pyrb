package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ReturnVisitor extends StatementVisitor {
    public ReturnVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitReturn_stmt(Python3Parser.Return_stmtContext ctx) {
        switch (context.getCurrentScope().getType()) {
            case GENERATOR:
                context.writeRuby("next");
                visitChildren(ctx);
                break;

            default:
                context.writeRuby("throw(:return");

                if (ctx.testlist() != null) {
                    if (ctx.children.size() > 1) {
                        context.writeRuby(", ");
                    }

                    if (ctx.testlist().test().size() > 1) {
                        context.writeRuby("[");
                    }

                    for (int i = 0; i < ctx.testlist().test().size(); i++) {
                        if (i > 0) {
                            context.writeRuby(", ");
                        }

                        visit(ctx.testlist().test(i));
                    }

                    if (ctx.testlist().test().size() > 1) {
                        context.writeRuby("]");
                    }
                }

                context.writeRubyLine(")");

                break;
        }

        return null;
    }
}
