package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class ArithmeticExpressionVisitor extends StatementVisitor {
    public ArithmeticExpressionVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitArith_expr(Python3Parser.Arith_exprContext ctx) {
        if (ctx.term().size() > 1) {
            for (ParseTree child : ctx.children) {
                if (child.getClass() == TerminalNodeImpl.class) {
                    context.writeRuby(Ruby.pad(child.getText()));
                } else {
                    visit(child);
                }
            }
        } else {
            // is this not where unary operators would happen?
            // apparently not :(
            visitChildren(ctx);
        }

        return null;
    }
}
