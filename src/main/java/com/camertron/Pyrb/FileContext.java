package com.camertron.Pyrb;

public class FileContext extends BaseContext {
    private boolean open;

    public FileContext(String filePath) {
        this.open = true;

        writeRubyLine("# encoding: utf-8\n");
        writeRubyLine("require 'pyrb'\n");

        writeRuby("module ");
        writeRubyLine(Ruby.modularize(filePath));
        enterScope(ScopeType.FILE);

        writeRubyLine("def self.location");
        enterScope(ScopeType.METHOD);
        writeRubyLine("__FILE__");
        exitScope();
        writeRubyLine("end\n");

        writeRubyLine("extend Pyrb::Runtime");
        writeRubyLine("using Pyrb::Runtime\n");
    }

    @Override
    public void enterBlock() {
        checkOpen();
        super.enterBlock();
    }

    @Override
    public void exitBlock() {
        checkOpen();
        super.exitBlock();
    }

    @Override
    public void enterScope(ScopeType type) {
        checkOpen();
        super.enterScope(type);
    }

    @Override
    public void exitScope() {
        checkOpen();
        super.exitScope();
    }

    @Override
    public void writeRuby(String ruby) {
        checkOpen();
        super.writeRuby(ruby);
    }

    @Override
    public void writeRubyLine(String ruby) {
        checkOpen();
        super.writeRubyLine(ruby);
    }

    @Override
    public void writeRubyLine() {
        checkOpen();
        super.writeRubyLine();
    }

    @Override
    public void ensureNewLine() {
        checkOpen();
        super.ensureNewLine();
    }

    @Override
    public String getRubyOutput() {
        if (open) {
            throw new RuntimeException("File context is still open");
        }

        return output.toString();
    }

    public void close() {
        writeRubyLine("Pyrb::Sys.exports['modules'][__FILE__] = self");
        exitScope();
        writeRubyLine("end");
        this.open = false;
    }

    private void checkOpen() {
        if (!open) {
            throw new RuntimeException("File context has been closed");
        }
    }
}
