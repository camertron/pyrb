package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class WhileVisitor extends StatementVisitor {
    public WhileVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitWhile_stmt(Python3Parser.While_stmtContext ctx) {
        context.writeRuby("while !!(");
        visit(ctx.test());
        context.writeRuby(")");
        context.enterBlock();
        context.writeRubyLine();

        for (Python3Parser.SuiteContext suite : ctx.suite()) {
            visit(suite);
        }

        context.exitBlock();
        context.writeRubyLine("end\n");

        return null;
    }
}
