package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class DictOrSetVisitor extends StatementVisitor {
    public DictOrSetVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitDictorsetmaker(Python3Parser.DictorsetmakerContext ctx) {
        boolean set = isSet(ctx);

        if (set) {
            context.writeRuby("Pyrb::Set.new([[");
        } else {
            context.writeRuby("Pyrb::Dict.new([{");
        }

        for (ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                if (child.getText().equals(":")) {
                    context.writeRuby(" => ");
                } else {
                    context.writeRuby(Ruby.pad(child.getText()));
                }
            } else {
                visit(child);
            }
        }

        if (set) {
            context.writeRuby("]])");
        } else {
            context.writeRuby("}])");
        }

        return null;
    }

    private static boolean isSet(Python3Parser.DictorsetmakerContext ctx) {
        for (ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                if (child.getText().equals(":")) {
                    return false;
                }
            }
        }

        return true;
    }
}
