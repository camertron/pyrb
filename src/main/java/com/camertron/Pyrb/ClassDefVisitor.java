package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class ClassDefVisitor extends StatementVisitor {
    public ClassDefVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitClassdef(Python3Parser.ClassdefContext ctx) {
        String className = ctx.NAME().getText();
        String rubyClassName = Ruby.constantize(className);

        if (context.getCurrentScope().getType() == ScopeType.FILE) {
            context.writeRuby("exports['");
            context.writeRuby(className);
            context.writeRuby("'] = ");
        } else {
            context.writeRuby(className);
            context.writeRuby(" ||= ");
        }

        context.writeRuby("Pyrb::PythonClass.new('");
        context.writeRuby(className);

        context.getSymbolTable().addSymbol(className, rubyClassName);

        context.writeRuby("', [");

        // superclasses
        if (ctx.arglist() != null) {
            for (int i = 0; i < ctx.arglist().argument().size(); i++) {
                if (i > 0) {
                    context.writeRuby(", ");
                }

                visit(ctx.arglist().argument(i));
            }
        }

        context.writeRuby("])");

        if (!ctx.suite().getText().trim().equals("pass")) {
            context.writeRubyLine(".tap do |klass|");

            context.enterScope(ScopeType.CLASS);
            context = BlockContext.enter(context);
            context.getCurrentScope().setClassRef(context.getSymbolTable().getSymbol(className));

            visit(ctx.suite());

            context = ((BlockContext)context).exit();
            context.exitScope();
            context.writeRubyLine("end\n");
        } else {
            context.writeRubyLine();
        }

        // define ruby constant
        if (context.getCurrentScope().getType() == ScopeType.FILE) {
            context.writeRuby(rubyClassName);
            context.writeRuby(" = exports['");
            context.writeRuby(className);
            context.writeRubyLine("']\n");
        }

        return null;
    }
}
