package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class StarExpressionVisitor extends StatementVisitor {
    public StarExpressionVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitStar_expr(Python3Parser.Star_exprContext ctx) {
        context.writeRuby("*");
        visit(ctx.expr());
        return null;
    }
}
