package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class FuncDefVisitor extends StatementVisitor {
    public FuncDefVisitor(BaseContext context) {
        super(context);
    }

    public Void visitFuncdef(Python3Parser.FuncdefContext ctx) {
        String name = ctx.NAME().getText();

        ArgListVisitor argList = new ArgListVisitor(context);

        if (ctx.parameters().typedargslist() != null) {
            argList.visit(ctx.parameters().typedargslist());
        }

        switch (context.getCurrentScope().getType()) {
            case FILE:
            case INVOCATION:
            case METHOD:
                defineFunction(name, argList.getArgs(), ctx);
                break;

            case CLASS:
                defineMethod(name, argList.getArgs(), ctx);
                break;
        }

        return null;
    }

    private void defineMethod(String name, ArgumentList args, Python3Parser.FuncdefContext ctx) {
        boolean isGen = isGenerator(ctx);
        String ref = "klass.attrs['" + name + "']";

        context.writeRuby(ref);
        context.writeRuby(" = Pyrb.defn(");
        context.writeRuby(args.toMetadata());

        context.writeRuby(") do |");
        context.writeRuby(args.toArgs());
        context.writeRubyLine("|");

        context.enterScope(ScopeType.METHOD);
        context = BlockContext.enter(context);

        // add args
        for (Argument arg : args) {
            context.getSymbolTable().addSymbol(arg.getSymbol());
        }

        context.getCurrentScope().setSelfRef(args.get(0).getSymbol());

        if (isGen) {
            context.writeRubyLine("Pyrb::Generator.new([");
            context.enterScope(ScopeType.INVOCATION);
            context.writeRubyLine("Enumerator.new do |__pyrb_gen__|");
            context.enterScope(ScopeType.GENERATOR);
        }

//        LocalsScanner locScanner = new LocalsScanner(context);
//        locScanner.visit(ctx.suite());
//        locScanner.apply();

        visit(ctx.suite());

        if (isGen) {
            context.exitScope();
            context.writeRubyLine("end");
            context.exitScope();
            context.writeRubyLine("])");
        }

        context = ((BlockContext)context).exit();
        context.exitScope();
        context.getSymbolTable().addSymbol(name, ref, SymbolType.DEF);

        context.ensureNewLine();
        context.writeRubyLine("end\n");
    }

    private void defineFunction(String name, ArgumentList args, Python3Parser.FuncdefContext ctx) {
        boolean isGen = isGenerator(ctx);

        String ref;

        if (context.getCurrentScope().getType() == ScopeType.FILE && !context.getSymbolTable().contains(name)) {
            context.writeRuby("exports['");
            context.writeRuby(name);
            context.writeRuby("'] = Pyrb.defn(");
            context.writeRuby(args.toMetadata());

            // so the function can call itself
            ref = "exports['" + name + "']";
            context.getSymbolTable().addSymbol(name, ref, SymbolType.DEF);
            context.writeRuby(") do |");
            context.writeRuby(args.toArgs());
            context.writeRubyLine("|");
        } else if (context.getCurrentScope().getType() == ScopeType.INVOCATION) {
            context.writeRuby("Pyrb.defn(");
            context.writeRuby(args.toMetadata());
            context.writeRuby(") do |");
            context.writeRuby(args.toArgs());
            context.writeRubyLine("|");

            // so the function can call itself
            // invocations happen for things like static and class method definitions, and should be
            // available on a local "klass" variable
            ref = "klass.attrs['" + name + "']";
            context.getSymbolTable().addSymbol(name, ref, SymbolType.DEF);
        } else {
            Symbol sym = context.getSymbolTable().getSymbol(name);

            if (sym != null) {
                name = sym.getRubyExpression();
            }

            context.writeRuby(name);
            context.writeRuby(" = Pyrb.defn(");;
            context.writeRuby(args.toMetadata());
            context.writeRuby(") do |");
            context.writeRuby(args.toArgs());
            context.writeRubyLine("|");

            // so the function can call itself
            ref = name;
            context.getSymbolTable().addSymbol(name, ref, SymbolType.DEF);
        }

        context.enterScope(ScopeType.METHOD);
        context = BlockContext.enter(context);

        for (Argument arg : args) {
            context.getSymbolTable().addSymbol(arg.getSymbol());
        }

        if (isGen) {
            context.writeRubyLine("Pyrb::Generator.new([");
            context.enterScope(ScopeType.INVOCATION);
            context.writeRubyLine("Enumerator.new do |__pyrb_gen__|");
            context.enterScope(ScopeType.GENERATOR);
        }

        visit(ctx.suite());

        if (isGen) {
            context.exitScope();
            context.writeRubyLine("end");
            context.exitScope();
            context.writeRubyLine("])");
        }

        context = ((BlockContext)context).exit();
        context.exitScope();
        context.getSymbolTable().addSymbol(name, ref);

        context.ensureNewLine();
        context.writeRubyLine("end\n");
    }

    private boolean isGenerator(Python3Parser.FuncdefContext ctx) {
        return ctx.suite().getText().contains("yield");
    }
}
