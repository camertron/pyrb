package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class StatementVisitor extends PyrbBaseVisitor {
    public StatementVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitClassdef(Python3Parser.ClassdefContext ctx) {
        return new ClassDefVisitor(context).visit(ctx);
    }

    @Override
    public Void visitFuncdef(Python3Parser.FuncdefContext ctx) {
        return new FuncDefVisitor(context).visit(ctx);
    }

    @Override
    public Void visitIf_stmt(Python3Parser.If_stmtContext ctx) {
        return new IfStatementVisitor(context).visit(ctx);
    }

    @Override
    public Void visitAtom_expr(Python3Parser.Atom_exprContext ctx) {
        return new AtomExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitRaise_stmt(Python3Parser.Raise_stmtContext ctx) {
        return new RaiseExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitComparison(Python3Parser.ComparisonContext ctx) {
        // For some reason the python grammar wraps a bunch of things up in this
        // comparison context even when the thing in question has nothing to do
        // with comparing. In such cases, there will be no comparison operator
        // (i.e. comp_op() will return null), so avoid creating a visitor.
        if (ctx.comp_op().size() > 0) {
            return new ComparisonVisitor(context).visit(ctx);
        }

        return this.visitChildren(ctx);
    }

    @Override
    public Void visitFor_stmt(Python3Parser.For_stmtContext ctx) {
        return new ForStatementVisitor(context).visit(ctx);
    }

    @Override
    public Void visitComp_for(Python3Parser.Comp_forContext ctx) {
        return new ForStatementVisitor(context).visit(ctx);
    }

    @Override
    public Void visitExpr(Python3Parser.ExprContext ctx) {
        return new ExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitStar_expr(Python3Parser.Star_exprContext ctx) {
        return new StarExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitExpr_stmt(Python3Parser.Expr_stmtContext ctx) {
        return new ExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitAugassign(Python3Parser.AugassignContext ctx) {
        return new AugAssignVisitor(context).visit(ctx);
    }

    @Override
    public Void visitReturn_stmt(Python3Parser.Return_stmtContext ctx) {
        return new ReturnVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTry_stmt(Python3Parser.Try_stmtContext ctx) {
        return new TryVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTerm(Python3Parser.TermContext ctx) {
        return new TermVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTestlist_comp(Python3Parser.Testlist_compContext ctx) {
        return new TestListCompVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTestlist_star_expr(Python3Parser.Testlist_star_exprContext ctx) {
        return new TestListVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTestlist(Python3Parser.TestlistContext ctx) {
        return new TestListVisitor(context).visit(ctx);
    }

    @Override
    public Void visitWith_stmt(Python3Parser.With_stmtContext ctx) {
        return new WithStatementVisitor(context).visit(ctx);
    }

    @Override
    public Void visitTest(Python3Parser.TestContext ctx) {
        return new TestVisitor(context).visit(ctx);
    }

    @Override
    public Void visitAnd_test(Python3Parser.And_testContext ctx) {
        return new TestVisitor(context).visit(ctx);
    }

    @Override
    public Void visitOr_test(Python3Parser.Or_testContext ctx) {
        return new TestVisitor(context).visit(ctx);
    }

    @Override
    public Void visitImport_stmt(Python3Parser.Import_stmtContext ctx) {
        // top-level (i.e. file scope level) imports are handled by the ImportExportScanner
        if (context.getCurrentScope().getType() != ScopeType.FILE || context.isInsideTry()) {
            ImportVisitor importVisitor = new ImportVisitor();
            importVisitor.visit(ctx);

            for (Import imp : importVisitor.getImports()) {
                Import importWithContext = imp.applyTo(context);
                context.writeRubyLine(importWithContext.toRuby());
            }
        }

        return null;
    }

    @Override
    public Void visitArith_expr(Python3Parser.Arith_exprContext ctx) {
        return new ArithmeticExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitLambdef(Python3Parser.LambdefContext ctx) {
        return new LambdaVisitor(context).visit(ctx);
    }

    @Override
    public Void visitLambdef_nocond(Python3Parser.Lambdef_nocondContext ctx) {
        return new LambdaVisitor(context).visit(ctx);
    }

    @Override
    public Void visitDictorsetmaker(Python3Parser.DictorsetmakerContext ctx) {
        return new DictOrSetVisitor(context).visit(ctx);
    }

    @Override
    public Void visitAnd_expr(Python3Parser.And_exprContext ctx) {
        return new AndExpressionVisitor(context).visit(ctx);
    }

    @Override
    public Void visitPower(Python3Parser.PowerContext ctx) {
        return new ExponentVisitor(context).visit(ctx);
    }

    @Override
    public Void visitDecorated(Python3Parser.DecoratedContext ctx) {
        return new DecorationVisitor(context).visit(ctx);
    }

    @Override
    public Void visitFactor(Python3Parser.FactorContext ctx) {
        return new FactorVisitor(context).visit(ctx);
    }

    @Override
    public Void visitYield_stmt(Python3Parser.Yield_stmtContext ctx) {
        return new YieldVisitor(context).visit(ctx);
    }

    @Override
    public Void visitWhile_stmt(Python3Parser.While_stmtContext ctx) {
        return new WhileVisitor(context).visit(ctx);
    }

    @Override
    public Void visitDel_stmt(Python3Parser.Del_stmtContext ctx) {
        return new DelVisitor(context).visit(ctx);
    }

    @Override
    public Void visitShift_expr(Python3Parser.Shift_exprContext ctx) {
        return new ShiftVisitor(context).visit(ctx);
    }

    @Override
    public Void visitContinue_stmt(Python3Parser.Continue_stmtContext ctx) {
        return new ContinueVisitor(context).visit(ctx);
    }

    @Override
    public Void visitBreak_stmt(Python3Parser.Break_stmtContext ctx) {
        return new BreakVisitor(context).visit(ctx);
    }

    @Override
    public Void visitAssert_stmt(Python3Parser.Assert_stmtContext ctx) {
        return new AssertVisitor(context).visit(ctx);
    }

    @Override
    public Void visitGlobal_stmt(Python3Parser.Global_stmtContext ctx) {
        return new GlobalVisitor(context).visit(ctx);
    }
}
