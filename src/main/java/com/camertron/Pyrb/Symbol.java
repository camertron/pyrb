package com.camertron.Pyrb;

public class Symbol {
    private String pythonName;
    private String rubyExpression;
    private SymbolType type;

    public static Symbol builtIn(String pythonName, String rubyExpression) {
        return new Symbol(pythonName, rubyExpression, SymbolType.BUILTIN);
    }

    public Symbol(String pythonName, String rubyExpression) {
        this.pythonName = pythonName;
        this.rubyExpression = rubyExpression;
        this.type = SymbolType.LOCAL;
    }

    public Symbol(String pythonName, String rubyExpression, SymbolType type) {
        this(pythonName, rubyExpression);
        this.type = type;
    }

    public void setType(SymbolType type) {
        this.type = type;
    }

    public String getPythonName() {
        return pythonName;
    }

    public String getRubyExpression() {
        return rubyExpression;
    }

    public SymbolType getType() {
        return type;
    }

    public Symbol clone() {
        return new Symbol(pythonName, rubyExpression, type);
    }
}
