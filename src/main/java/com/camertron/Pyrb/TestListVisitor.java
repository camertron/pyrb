package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

public class TestListVisitor extends StatementVisitor {
    public TestListVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitTestlist_star_expr(Python3Parser.Testlist_star_exprContext ctx) {
        for(ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                context.writeRuby(Ruby.pad(child.getText()));
            } else {
                visit(child);
            }
        }

        return null;
    }

    @Override
    public Void visitTestlist(Python3Parser.TestlistContext ctx) {
        for(ParseTree child : ctx.children) {
            if (child.getClass() == TerminalNodeImpl.class) {
                context.writeRuby(Ruby.pad(child.getText()));
            } else {
                visit(child);
            }
        }

        return null;
    }
}
