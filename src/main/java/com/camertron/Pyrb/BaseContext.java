package com.camertron.Pyrb;

import java.util.Stack;

public class BaseContext {
    private static final String INDENT = "  ";

    protected Stack<Scope> scopeStack;
    protected int indentLevel;
    protected String indentStr;
    protected StringBuilder output;
    protected StringBuilder heredocBuffer;
    protected boolean startedLine;
    protected boolean insideTry;

    public BaseContext() {
        init();
        this.scopeStack = new Stack<Scope>();
        this.scopeStack.push(new Scope(ScopeType.FILE, SymbolTable.getBaseTable()));
    }

    public BaseContext(Stack<Scope> scopeStack) {
        init();
        this.scopeStack = scopeStack;
        // local scope stack?
    }

    private void init() {
        this.indentLevel = 0;
        this.indentStr = "";
        this.output = new StringBuilder();
        this.heredocBuffer = new StringBuilder();
        this.startedLine = false;
        this.insideTry = false;
    }

    public Scope getCurrentScope() {
        return scopeStack.peek();
    }

    public Scope getParentScope() {
        if (scopeStack.size() > 1) {
            return scopeStack.get(scopeStack.size() - 2);
        } else {
            return null;
        }
    }

    public SymbolTable getSymbolTable() {
        return getCurrentScope().getSymbolTable();
    }

    public void enterBlock() {
        enterBlock(true);
    }

    public void enterBlock(boolean indent) {
        if (indent) {
            this.indent();
        }
    }

    public void exitBlock() {
        exitBlock(true);
    }

    public void exitBlock(boolean dedent) {
        if (dedent) {
            this.dedent();
        }
    }

    public boolean isInsideTry() {
        return insideTry;
    }

    public void setInsideTry(boolean newValue) {
        insideTry = newValue;
    }

    private void indent() {
        indentLevel ++;
        indentStr += INDENT;
    }

    private void dedent() {
        indentLevel --;
        indentStr = indentStr.substring(0, indentStr.length() - INDENT.length());
    }

    public void enterScope(ScopeType type) {
        enterScope(type, true);
    }

    public void enterScope(ScopeType type, boolean indent) {
        enterBlock(indent);
        scopeStack.push(new Scope(type, getCurrentScope()));
    }

    public void exitScope() {
        exitScope(true);
    }

    public void exitScope(boolean dedent) {
        exitBlock(dedent);
        scopeStack.pop();
    }

    public void writeRuby(String ruby) {
        if (!startedLine) {
            output.append(indentStr);
            startedLine = true;
        }

        output.append(indentNewlines(ruby));
    }

    public void writeRubyLine(String ruby) {
        if (!startedLine) {
            output.append(indentStr);
        }

        output.append(indentNewlines(ruby));
        output.append("\n");
        startedLine = false;
        flushHeredoc();
    }

    public void writeRubyLine() {
        output.append("\n");
        startedLine = false;
        flushHeredoc();
    }

    public void ensureNewLine() {
        if (startedLine) {
            output.append("\n");
            startedLine = false;
        }
    }

    public void writeHeredoc(String contents) {
        heredocBuffer.append(contents);
    }

    public String getRubyOutput() {
        return output.toString();
    }

    private void flushHeredoc() {
        if (heredocBuffer.length() > 0) {
            ensureNewLine();
            output.append(indentStr);
            output.append(indentNewlines(heredocBuffer.toString()));
            output.append("\n");
            heredocBuffer.delete(0, heredocBuffer.length());
        }
    }

    private String indentNewlines(String str) {
        return str
            .replaceAll("\\r?\\n", "\n" + indentStr)
            .replaceAll("\n[\\s]*\n", "\n\n")
            .replaceAll("\n +\\z", "\n");
    }
}
