package com.camertron.Pyrb;

import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public interface IDecorator {
    void applyTo(BaseContext context);
    IDecorator getSubDecorator(ArrayList<String> subNames, ParseTree body);
}
