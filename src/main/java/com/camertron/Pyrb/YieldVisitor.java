package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

public class YieldVisitor extends StatementVisitor {
    public YieldVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitYield_stmt(Python3Parser.Yield_stmtContext ctx) {
        if (ctx.yield_expr().yield_arg().children.get(0).getText().equals("from")) {
            // indicates a "yield from" statement
            visit(ctx.yield_expr());
            context.writeRubyLine(".each { |item| __pyrb_gen__ << item }");
        } else {
            boolean yieldingList = false;

            for (ParseTree child : ctx.yield_expr().yield_arg().testlist().children) {
                if (child.getText().equals(",")) {
                    yieldingList = true;
                    break;
                }
            }

            context.writeRuby("__pyrb_gen__ << ");

            if (yieldingList) {
                context.writeRuby("[");
            }

            visit(ctx.yield_expr());

            if (yieldingList) {
                context.writeRuby("]");
            }

            context.ensureNewLine();
        }

        return null;
    }
}
