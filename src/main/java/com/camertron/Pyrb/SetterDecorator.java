package com.camertron.Pyrb;

import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public class SetterDecorator implements IDecorator {
    private PropertyDecorator parent;
    private ParseTree funcDef;

    public SetterDecorator(PropertyDecorator parent, ParseTree funcDef) {
        this.parent = parent;
        this.funcDef = funcDef;
    }

    public void applyTo(BaseContext context) {
        context.writeRuby(parent.getContextRef());
        context.writeRuby(".properties['");
        context.writeRuby(parent.getFuncDef().NAME().getText());
        context.writeRuby("'].setter = ");
        context.enterScope(ScopeType.INVOCATION, false);
        new FuncDefVisitor(context).visit(funcDef);
        context.exitScope(false);
    }

    public IDecorator getSubDecorator(ArrayList<String> subNames, ParseTree body) {
        throw new RuntimeException("Sub-decorator '" + subNames.get(0) + "' isn't supported by the property setter decorator");
    }
}
