package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class LambdaVisitor extends StatementVisitor {
    public LambdaVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitLambdef(Python3Parser.LambdefContext ctx) {
        context.writeRuby("Pyrb.defn(");

        if (ctx.varargslist() != null) {
            ArgListVisitor argList = new ArgListVisitor(context);
            argList.visit(ctx.varargslist());
            context.writeRuby(argList.getArgs().toMetadata());
            context.writeRuby(") { |");
            context.writeRuby(argList.getArgs().toArgs());
            context.writeRuby("| ");

            context.enterScope(ScopeType.BLOCK, false);

            for (Argument arg : argList.getArgs()) {
                context.getSymbolTable().addSymbol(arg.getSymbol());
            }
        } else {
            context.writeRuby(") { ");
            context.enterScope(ScopeType.BLOCK, false);
        }

        // this is the body, I have no idea why it's called a "test"
        visit(ctx.test());
        context.exitScope(false);
        context.writeRuby(" }");
        return null;
    }

    @Override
    public Void visitLambdef_nocond(Python3Parser.Lambdef_nocondContext ctx) {
        // TODO: figure out WTF this is and how it's different from a regular lambda
        return null;
    }
}
