package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class DelVisitor extends StatementVisitor {
    public DelVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitDel_stmt(Python3Parser.Del_stmtContext ctx) {
        Python3Parser.Atom_exprContext atomExpr = findNextAtomExpr(ctx.exprlist());
        AtomExpressionVisitor atomVis = new AtomExpressionVisitor(context);

        if (atomExpr.trailer().size() == 0) {
            Symbol sym = context.getSymbolTable().getSymbol(atomExpr.atom().getText());
            String target;
            String name;

            if (sym == null) {
                target = "exports";
                name = "'" + atomExpr.atom().getText() + "'";
            } else {
                // pray to god these are always subscripts a la exports["name"]
                // I mean, they should be... famous last words
                int idx = sym.getRubyExpression().indexOf("[");
                target = sym.getRubyExpression().substring(0, idx);
                name = sym.getRubyExpression().substring(idx + 1, sym.getRubyExpression().length() - 1);
            }

            context.writeRuby(target);
            context.writeRuby(".del(");
            context.writeRuby(name);
            context.writeRubyLine(")");
        } else {
            atomVis.visitAtom(atomExpr.atom());

            atomVis.emitTrailerList(atomExpr, new TrailerShapeBuilder(atomExpr).allButLastTrailer());
            context.writeRuby(".del(");

            // if the last trailer isn't a subscript, it must be dot access, so enter in quotes
            // eg del foo.bar becomes foo.del('bar')
            boolean isDotAccess = false;

            if (atomExpr.trailer(atomExpr.trailer().size() - 1).subscriptlist() == null) {
                isDotAccess = true;
                context.writeRuby("'");
            }

            atomVis.emitTrailerList(atomExpr, new TrailerShapeBuilder(atomExpr).lastWithoutSubscript());

            if (isDotAccess) {
                context.writeRuby("'");
            }

            context.writeRubyLine(")");
        }

        return null;
    }
}
