package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;

public class BreakVisitor extends StatementVisitor {
    public BreakVisitor(BaseContext context) {
        super(context);
    }

    @Override
    public Void visitBreak_stmt(Python3Parser.Break_stmtContext ctx) {
        if (context.getCurrentScope().isInScope(ScopeType.FOR_LOOP_WITH_ELSE)) {
            context.writeRubyLine("throw :break, true");
        } else {
            context.writeRubyLine("break");
        }

        return null;
    }
}
