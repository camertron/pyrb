package com.camertron.Pyrb;

import com.camertron.PythonParser.Python3Parser;
import org.antlr.v4.runtime.tree.ParseTree;

public class IfStatementVisitor extends StatementVisitor {
    public IfStatementVisitor(BaseContext context) {
        super(context);
    }

    // You may be wondering why !! is added to every if statement. Well, for the simple reason that,
    // in Python, 0 == False and 1 == True. In the pyrb runtime, the Integer, TrueClass, and FalseClass
    // classes have been modified so these equivalences (and their associative inverses) hold, but
    // unfortunately that only gets us so far. In Ruby, it's impossible to define another object as
    // being falsey - the built-in "false" and "nil" values are the only falsey values we get. Consider
    // these two examples:
    //
    // Ruby: if 0              Python: if 0:
    //         puts 'abc'                print('abc')
    //       end
    //
    // Although at first glance equivalent, the Ruby version will print "abc" while the Python version
    // will not. In Ruby, 0 is not nil or false, so it must be true. In Python, 0 evaluates to false.
    // To mitigate the problem, the pyrb runtime redefines the ! method on Integer. Calling it twice
    // will effectively cast the value to a boolean, respecting the Python rules in the process.
    //
    public Void visitIf_stmt(Python3Parser.If_stmtContext ctx) {
        for (ParseTree child : ctx.children) {
            if (child.getText().equals("if")) {
                context.writeRuby("if !!(");
            } else if (child.getText().equals("elif")) {
                context.ensureNewLine();
                context.writeRuby("elsif !!(");
            } else if (child.getText().equals("else")) {
                context.ensureNewLine();
                context.writeRubyLine("else");
                context.enterBlock();
            } else if (child.getClass() == Python3Parser.TestContext.class) {
                visit(child);
                context.writeRubyLine(")");
                context.enterBlock();
            } else if (child.getClass() == Python3Parser.SuiteContext.class) {
                visit(child);
                context.exitBlock();
            }
        }

        context.ensureNewLine();
        context.writeRubyLine("end\n");
        return null;
    }
}
